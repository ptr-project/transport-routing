include_guard(GLOBAL)

option(ROUTING_CCACHE "Use ccache for compilation" OFF)

if(NOT ROUTING_CCACHE)
    message(STATUS "Not using ccache as requested")
    return()
endif()

find_program(CCACHE_BIN ccache REQUIRED)
if(NOT CCACHE_BIN)
    message(SEND_ERROR "Could not find ccache")
endif()

set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ${CCACHE_BIN})
set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ${CCACHE_BIN})
