include_guard(GLOBAL)

find_package(Boost REQUIRED)

if (BUILD_TESTING)
    find_package(catch2_extended REQUIRED)
endif()

find_package(nlohmann_json REQUIRED)

find_package(sqlite_utils REQUIRED)

find_package(SQLite3 REQUIRED)

find_package(pybind11 REQUIRED)

find_package(units REQUIRED)
target_compile_definitions(units::units INTERFACE "DISABLE_PREDEFINED_UNITS")

find_package(date REQUIRED)

# Resetting _POSTFIX to sidestep issue #25
string(TOUPPER "${CMAKE_BUILD_TYPE}" buildtype_upper)
set(CMAKE_${buildtype_upper}_POSTFIX "")
message(STATUS "Resetting CMAKE_${buildtype_upper}_POSTFIX")

if(NOT DEFINED PYTHON_EXECUTABLE)
  message(FATAL_ERROR "Cannot find python executable")
endif()

# FIXME this might be brittle. Currently, it return the expected paths
# on alpine (/usr/local/lib/python3.x/site-packages)
# and ubuntu (/usr/local/lib/python3.x/dist-packages).
execute_process(COMMAND "${PYTHON_EXECUTABLE}" -c "import site; print(site.getsitepackages()[0])"
                OUTPUT_VARIABLE python_lib_path
                OUTPUT_STRIP_TRAILING_WHITESPACE
                COMMAND_ERROR_IS_FATAL ANY)

if(NOT EXISTS "${python_lib_path}")
  message(FATAL_ERROR "The python purelib path '${python_lib_path}' does not exist")
endif()

set(PYTHON_LIB_PATH "${python_lib_path}" CACHE PATH "Python Library Path")
