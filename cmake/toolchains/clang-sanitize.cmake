include_guard(GLOBAL)

include(${CMAKE_CURRENT_LIST_DIR}/clang.cmake)

set(CMAKE_CXX_FLAGS -fsanitize=address,undefined CACHE STRING "" FORCE)
set(CMAKE_C_FLAGS -fsanitize=address,undefined CACHE STRING "" FORCE)
