include_guard(GLOBAL)

enable_testing()

option(ROUTING_TEST_JUNIT "Redirect test output to JUnit xml files" OFF)

function(add_routing_test)
  set(EXTRA_ARGS)
  if (ROUTING_TEST_JUNIT)
    set(EXTRA_ARGS -o ${CMAKE_CURRENT_BINARY_DIR}/TEST-${PROJECT_NAME}.xml -r junit)
  endif()
  add_test(${PROJECT_NAME} ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${PROJECT_NAME} ${EXTRA_ARGS})
endfunction()
