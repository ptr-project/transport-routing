include_guard(GLOBAL)

# Source:
# https://discourse.cmake.org/t/cmake-list-of-all-project-targets/1077/7
# Overriding a cmake funtion is undefined behavior, therefore not using the
# approach of overwrite project, add_executable, add_library

set(ProjectDirectories "")
get_property(stack DIRECTORY ${CMAKE_SOURCE_DIR} PROPERTY SUBDIRECTORIES)
while(stack)
  list(POP_BACK stack directory)
  list(APPEND ProjectDirectories ${directory})
  get_property(subdirs DIRECTORY ${directory} PROPERTY SUBDIRECTORIES)
  if(subdirs)
    list(APPEND stack ${subdirs})
  endif()
endwhile()

function(getTargets foundTargets)
  foreach(dir ${ProjectDirectories})
    get_property(target DIRECTORY ${dir} PROPERTY BUILDSYSTEM_TARGETS)
    list(APPEND targets ${target})
  endforeach()
  set(${foundTargets} ${targets} PARENT_SCOPE)
endfunction()

function(getSources foundSources)
  foreach(target ${AllTargets})
    get_property(targetType TARGET ${target} PROPERTY TYPE)
    if (NOT ("${targetType}" STREQUAL "INTERFACE_LIBRARY"))
      get_property(sources TARGET ${target} PROPERTY SOURCES)
      get_property(targetSourceDir TARGET ${target} PROPERTY SOURCE_DIR)
      foreach(source ${sources})
        if (IS_ABSOLUTE "${source}")
          list(APPEND allSources "${source}")
        else()
          set(result "${targetSourceDir}/${source}")
          list(APPEND allSources "${result}")
        endif()
      endforeach()
    endif()
    get_property(interfaceSources TARGET ${target} PROPERTY INTERFACE_SOURCES)
    list(APPEND allSources ${interfaceSources})
  endforeach()
  set(${foundSources} ${allSources} PARENT_SCOPE)
endfunction()

# ------------- Check Targets -------------
getTargets(AllTargets)

foreach(target ${AllTargets})
  if (NOT ("${target}" MATCHES "^[a-z_0-9]+$"))
    message(SEND_ERROR "${target} target name does not consist of lower case letters and underscores")
  endif()
endforeach()

# ------------- Check Sources -------------

getSources(AllSources)

foreach(source ${AllSources})
  get_filename_component(filename "${source}" NAME)
  if (NOT ("${filename}" MATCHES "^[a-z_]+\.(cpp|h)$"))
    message(SEND_ERROR "Filename for absolute path ${source} does not consist of lower case letters, underscore, and the extension cpp or h.")
  endif()
endforeach()

