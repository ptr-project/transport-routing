include_guard(GLOBAL)

option(ROUTING_MAP "Download current map" ON)
set(ROUTING_MAP_CACHE_DIR "$ENV{HOME}/.routingmapcache" CACHE FILEPATH "Where to store downloaded maps")

set(ROUTING_MAP_URL "https://gitlab.com/ptr-project/ptr-maps/-/package_files/10161692/download")
set(ROUTING_MAP_SHA1 "0ae62c7e5a2d2d0d2b3b3aa893f247562a9cb221")

if (NOT ROUTING_MAP)
  message(STATUS "ROUTING_MAP disabled, skipping map-based tests")
  function(add_map_test TARGET_NAME)
    # NOP
  endfunction()

  return()
endif()

include(ExternalProject)

ExternalProject_Add(map
  URL "${ROUTING_MAP_URL}"
  URL_HASH "SHA1=${ROUTING_MAP_SHA1}"
  DOWNLOAD_DIR "${ROUTING_MAP_CACHE_DIR}"
  CONFIGURE_COMMAND ""
  BUILD_COMMAND ""
  INSTALL_COMMAND ""
)

ExternalProject_Get_Property(map SOURCE_DIR)
set_target_properties(map PROPERTIES MAP_PATH "${SOURCE_DIR}/map.sqlite")

function(add_map_test TARGET_NAME)
  add_test(NAME ${TARGET_NAME}
    COMMAND /bin/sh -c "ROUTING_MAP=$<TARGET_PROPERTY:map,MAP_PATH> $<TARGET_FILE:${TARGET_NAME}>"
  )
endfunction()
