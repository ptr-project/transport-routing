include_guard(GLOBAL)

option(ENSURE_ABI "Track and compare ABI changes" OFF)

if(NOT ENSURE_ABI)
  function(ensure_abi)
  endfunction()
  return()
endif()

find_program(ABIDIFF_CMD abidiff)

if (NOT ABIDIFF_CMD)
  message(SEND_ERROR "Missing abidiff binary.")
endif()

find_program(ABIDW_CMD abidw)

if (NOT ABIDW_CMD)
  message(SEND_ERROR "Missing abidw binary.")
endif()

function(ensure_abi NAME LIBRARY)
  set(before_abi ${CMAKE_CURRENT_SOURCE_DIR}/${NAME}.abi.xml)
  set(after_abi ${CMAKE_CURRENT_BINARY_DIR}/${NAME}.abi.xml)

  add_test(NAME "ensure_abi_${NAME}"
    COMMAND bash -c "${ABIDW_CMD} --annotate \
    ${LIBRARY} \
    --out-file ${after_abi} \
    && \
    ${ABIDIFF_CMD} ${before_abi} ${after_abi}")
endfunction()
