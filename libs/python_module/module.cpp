/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <algorithm>
#include <env_helper/mappath.h>
#include <graph/gtfs_graph.h>
#include <graph/types.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <router/router.h>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

namespace python {

struct Coordinate {
  double lon_;
  double lat_;
};

struct Station {
  std::string station_;
};

namespace {
graph::TPlace Convert(const Coordinate& coordinate) {
  return graph::Point{coordinate.lon_, coordinate.lat_};
}

graph::TPlace Convert(const Station& station) {
  return graph::NodeId{station.station_};
}

const auto kBudget = 20000;  // pop counts
}  // namespace

using TGraph = graph::GTFSGraph;

std::string SearchFront(const graph::TPlace& from, const graph::TPlace& to,
                        const int timestamp) {
  // TODO avoid reopening the database for every request
  const TGraph graph{env_helper::GetMapPathFromEnv()};

  std::ostringstream searchSpaceGeoJson{};

  const auto kDepartureTime = graph::TTimePoint(graph::TSeconds{timestamp});

  auto routerArgs = router::Router::WithArgs(graph)
                        .Departure(from)
                        .Destination(to)
                        .DepartureTime(kDepartureTime)
                        .SearchSpaceStream(searchSpaceGeoJson);

  router::Router router(std::move(routerArgs));

  router.FindRoute(kBudget);

  return searchSpaceGeoJson.str();
}

std::string Route(const graph::TPlace& from, const graph::TPlace& to,
                  const int timestamp) {
  const TGraph graph{env_helper::GetMapPathFromEnv()};

  const auto kDepartureTime = graph::TTimePoint(graph::TSeconds{timestamp});

  router::Router router(router::Router::WithArgs(graph)
                            .Departure(from)
                            .Destination(to)
                            .DepartureTime(kDepartureTime));

  if (!router.FindRoute(kBudget)) {
    return "No Route Found";
  }

  return router.RouteToString();
}

std::vector<std::string> StopsWithName(const std::string& substr) {
  const TGraph graph{env_helper::GetMapPathFromEnv()};

  const auto& namedStations = graph.StopsWithName(substr);
  std::vector<std::string> stationsWithNames;
  stationsWithNames.reserve(namedStations.size());
  std::transform(namedStations.begin(), namedStations.end(),
                 std::back_inserter(stationsWithNames),
                 [](const auto& namedStation) {
                   return namedStation.id_.Id() + ", " + namedStation.name_;
                 });

  return stationsWithNames;
}

namespace py = pybind11;

// NOLINTNEXTLINE
PYBIND11_MODULE(transport_router, m) {
  py::class_<Coordinate>(m, "Coordinate")
      .def(py::init<double, double>(), py::arg("lon"), py::arg("lat"))
      .def_readonly("lon", &Coordinate::lon_)
      .def_readonly("lat", &Coordinate::lat_);

  py::class_<Station>(m, "Station")
      .def(py::init<std::string>(), py::arg("station_id"))
      .def_readonly("station_id", &Station::station_);

  m.doc() = "Transport Routing";

  // TODO duplication for the variant
  m.def(
      "searchfront",
      [](const Coordinate& from, const Coordinate& to, const int time_t) {
        return SearchFront(Convert(from), Convert(to), time_t);
      },
      "Show search front for route between two coordinates",
      py::arg("fromCoordinate"), py::arg("toCoordinate"), py::arg("time_t"));
  m.def(
      "searchfront",
      [](const Station& from, const Station& to, const int time_t) {
        return SearchFront(Convert(from), Convert(to), time_t);
      },
      "Show search front for route between two stations",
      py::arg("fromStation"), py::arg("toStation"), py::arg("time_t"));

  // TODO duplication for the variant
  m.def(
      "route",
      [](const Coordinate& from, const Coordinate& to, const int time_t) {
        return Route(Convert(from), Convert(to), time_t);
      },
      "Calculate route between two coordinates", py::arg("fromCoordinate"),
      py::arg("toCoordinate"), py::arg("time_t"));
  m.def(
      "route",
      [](const Station& from, const Station& to, const int time_t) {
        return Route(Convert(from), Convert(to), time_t);
      },
      "Calculate route between two stations", py::arg("fromCoordinate"),
      py::arg("toCoordinate"), py::arg("time_t"));

  m.def("stops_with_name", &StopsWithName, "Find Stops With a given name",
        py::arg("substr"));
}

}  // namespace python
