/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "env_helper/mappath.h"  // for GetMapPathFromEnv, kMapEnvironmentVa...

#include <catch2/catch.hpp>

#include <cstdlib>  // for unsetenv, setenv

namespace env_helper {
namespace {
bool SetMapEnvVariable(const char* value) {
  const auto overwrite = 1;
  const auto res = setenv(kMapEnvironmentVariableName, value, overwrite);
  return res == 0;
}
}  // namespace

namespace test {
using namespace Catch::Matchers;

TEST_CASE("Environment variable is read correctly") {
  const auto* expectedMapPath = "/tmp/itworks";
  REQUIRE(SetMapEnvVariable(expectedMapPath));
  REQUIRE_THAT(GetMapPathFromEnv(), Equals(expectedMapPath));
}

TEST_CASE("Empty environment variable throws") {
  REQUIRE(SetMapEnvVariable(""));
  REQUIRE_THROWS_WITH(GetMapPathFromEnv(), Contains("is empty"));
}

TEST_CASE("Unset environment variable throws") {
  REQUIRE(unsetenv(kMapEnvironmentVariableName) == 0);
  REQUIRE_THROWS_WITH(GetMapPathFromEnv(), Contains("is not set"));
}
}  // namespace test
}  // namespace env_helper
