/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "graph/gtfs_graph.h"
#include "graph/test/fixture/inserter.h"
#include "graph/types_io.h"

#include <catch2/catch.hpp>
#include <catch2_extended/custom_macros.h>

#include <sstream>
#include <string>

namespace graph::test {

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Graph Benchmarks -- Trains") {
  // last value is inspired by the Berlin GTFS dataset size
  const auto size = GENERATE(values({1000u, 5'250'000u}));
  const auto matchingSize = GENERATE(values({0u, 1u, 32u}));

  const fixture::TripEdge matching{.departureStop_ = "matching"};
  const fixture::TripEdge nonMatching{.departureStop_ = "nonMatching"};

  const GTFSGraph graph{fixture::PopulateDatabaseWith(
      {{matchingSize, matching}, {size - matchingSize, nonMatching}})};

  BENCHMARK_STABLE_STATISTICS("Retrieve Trains")
  (auto meter) {
    meter.measure([&] {
      auto result = graph.RetrieveTrains(
          NodeId{matching.departureStop_},
          std::chrono::system_clock::from_time_t(matching.departureTime_));
      CHECK(result.size() == matchingSize);
      meter["size"] = size;
      meter["matchingSize"] = matchingSize;
      return result;
    });
  };
}

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Graph Benchmarks -- Transfers") {
  // last value is inspired by the Berlin GTFS dataset size
  const auto size = GENERATE(values({1024u, 120'000u}));
  const auto matchingSize = GENERATE(values({0u, 1u, 32u}));

  const fixture::TransferEntry matching{.fromStopId_ = "matching",
                                        .toStopId_ = "target"};
  const fixture::TransferEntry nonMatching{.fromStopId_ = "nonMatching",
                                           .toStopId_ = "target"};

  auto db = fixture::PopulateDatabaseWith(
      {{matchingSize, matching}, {size - matchingSize, nonMatching}});
  fixture::DatabaseWith(db, {fixture::StopEntry{.stopId_ = "matching"},
                             fixture::StopEntry{.stopId_ = "target"}});
  const GTFSGraph graph{std::move(db)};

  BENCHMARK_STABLE_STATISTICS("Retrieve Transfers")
  (auto meter) {
    meter.measure([&] {
      auto result = graph.RetrieveTransfers(NodeId{matching.fromStopId_});
      CHECK(result.size() == matchingSize);
      meter["size"] = size;
      meter["matchingSize"] = matchingSize;
      return result;
    });
  };
}

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Graph Benchmark -- Stations") {
  SECTION("Retrieve Stations") {
    // last value is inspired by the Berlin GTFS dataset size
    const auto size = GENERATE(values({1024u, 41'000u}));
    const auto matchingSize = GENERATE(values({0u, 1u, 32u}));

    const fixture::StopEntry matching{.stopLat_ = 52.434447,
                                      .stopLon_ = 13.542970};
    const fixture::StopEntry nonMatching{.stopLat_ = 0, .stopLon_ = 0};

    const GTFSGraph graph{fixture::PopulateDatabaseWith(
        {{matchingSize, matching}, {size - matchingSize, nonMatching}})};

    BENCHMARK_STABLE_STATISTICS("Retrieve Stations Around Coordinate")
    (auto meter) {
      meter.measure([&] {
        const auto dummyRadius = 100;
        auto result = graph.StopsAround(
            Point{.lon_ = 13.542970, .lat_ = 52.434447}, dummyRadius);
        CHECK(result.size() == matchingSize);
        meter["size"] = size;
        meter["matchingSize"] = matchingSize;
        return result;
      });
    };
  }
}
}  // namespace graph::test
