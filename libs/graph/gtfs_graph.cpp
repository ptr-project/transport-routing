/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "graph/gtfs_graph.h"

#include "graph/detail/constants.h"        // for Constants
#include "graph/detail/date_handling.h"    // for CreateWeekdayMask
#include "graph/detail/geometry_helper.h"  // for GeometryHelper
#include "graph/distance.h"                // for Distance
#include "graph/edge.h"                    // for Edge, operator<<
#include "graph/node.h"                    // for NodeId, operator<<
#include "graph/types.h"  // for Point, TTimePoint, TTripId, TRouteId
#include "overloaded_visitor/overloaded.h"

#include <sqlite3.h>
#include <sqlite_utils/binding.h>             // for Binding
#include <sqlite_utils/database.h>            // for Database
#include <sqlite_utils/prepared_statement.h>  // for PreparedStatement
#include <sqlite_utils/result_row.h>          // for Row
#include <sqlite_utils/result_set.h>          // for ResultSet

#include <algorithm>    // for transform
#include <cassert>      // for assert
#include <chrono>       // for seconds
#include <iostream>     // for operator<<, basic_ostream
#include <iterator>     // for std::back_inserter
#include <numeric>      // for partial_sum
#include <sstream>      // for stringstream
#include <stdexcept>    // for runtime_error
#include <string>       // for char_traits, allocator, operator+, basic...
#include <type_traits>  // for enable_if<>::type, integral_constant
#include <utility>      // for make_unique, move
#include <variant>
#include <vector>  // for vector

namespace graph {

using PreparedStatement = sqlite_utils::PreparedStatement;
using Database = sqlite_utils::Database;
using Constants = detail::Constants;
using TEdges = std::vector<Edge>;

struct GTFSGraph::Impl {
  explicit Impl(const char* aDbPath) : db_{aDbPath} {
    assert(
        sqlite3_threadsafe() == 0 &&
        "We don't require thread safety and don't want to pay the penalty for "
        "the mutex code");
  }

  explicit Impl(Database&& database) : db_{std::move(database)} {}

  TEdges RetrieveTrains(TTimePoint arrivalAtTail, const NodeId& node) const;

  TEdges RetrieveTransfers(const NodeId& tailStopId) const;

  Point RetrieveCoordinate(const NodeId& node) const;

  TNodes StopsAround(const Point& coordinate, double radiusMeter) const;

  TNamedNodes StopsWithName(const std::string& substr) const;

  int InternalExecutionSteps() const;

 public:
  Database db_;
  // TODO rethink mutable
  mutable PreparedStatement outArcsPrepStmt_{db_, Constants::kStopsQuery};
  mutable PreparedStatement stopNamePrepStmt_{db_,
                                              Constants::kStationNameQuery};
  mutable PreparedStatement transfersPrepStmt_{db_, Constants::kTransfersQuery};
  mutable PreparedStatement tripNamePrepStmt_{db_, Constants::kTripNameQuery};
  mutable PreparedStatement randomNodePrepStmt_{db_,
                                                Constants::kRandomNodeQuery};
  mutable PreparedStatement stopCoordinatesPrepStmt_{
      db_, Constants::kStopCoordinatesQuery};
  mutable PreparedStatement reachableStopsPrepStmt_{
      db_, Constants::kReachableStopsQuery};
  mutable PreparedStatement stopsWithNamePrepStmt_{
      db_, Constants::kStopsWithSubstrQuery};
};

GTFSGraph::GTFSGraph(const char* aDbPath)
    : pImpl_{std::make_unique<Impl>(aDbPath)} {}

GTFSGraph::GTFSGraph(sqlite_utils::Database&& database)
    : pImpl_{std::make_unique<Impl>(std::move(database))} {}

GTFSGraph::~GTFSGraph() = default;

GTFSGraph::GTFSGraph(GTFSGraph&& graph) noexcept = default;

GTFSGraph& GTFSGraph::operator=(GTFSGraph&& graph) noexcept = default;

Graph::TEdges GTFSGraph::RetrieveTrains(const NodeId& nodeId,
                                        TTimePoint arrivalTime) const {
  return pImpl_->RetrieveTrains(arrivalTime, nodeId);
}

Graph::TEdges GTFSGraph::RetrieveTransfers(const NodeId& nodeId) const {
  return pImpl_->RetrieveTransfers(nodeId);
}

Graph::TEdges GTFSGraph::Impl::RetrieveTrains(const TTimePoint arrivalAtTail,
                                              const NodeId& node) const {
  // TODO I hate clocks. DST is not handled correctly.
  const auto secondsOfDay = detail::GetSecondsOfDay(arrivalAtTail);
  const auto dayEpochOffset = arrivalAtTail - secondsOfDay;

  sqlite_utils::Binding scopedBinding(
      outArcsPrepStmt_, {{Constants::kTailStopIdParameterName, node.Id()},
                         {Constants::kArrivalAtTailSecondsParameterName,
                          static_cast<int>(secondsOfDay.count())},
                         {Constants::kArrivalAtTailDateParameterName,
                          detail::GetDate(arrivalAtTail)},
                         {Constants::kWeekdayMaskParameterName,
                          detail::CreateWeekDayMaskChar(arrivalAtTail)}});

  // TODO ensure edges are not added multiple times
  // Currently, this is not possible as we settle nodes early on, though.

  sqlite_utils::ResultSet resultSet(outArcsPrepStmt_);
  TEdges result;
  std::transform(
      resultSet.begin(), resultSet.end(), std::back_inserter(result),
      [dayEpochOffset, node](const sqlite_utils::Row& row) {
        const auto departureSeconds = row["departure_time"].Integer();
        const auto departureTime =
            dayEpochOffset + std::chrono::seconds{departureSeconds};
        const auto arrivalSeconds = row["arrival_time"].Integer();
        const auto arrivalTime =
            dayEpochOffset + std::chrono::seconds{arrivalSeconds};
        // TODO dangerous, could be switched easily
        const auto headCoordinate =
            Point{row["head_lon"].Float(), row["head_lat"].Float()};
        return Edge{.from_ = node,
                    .to_ = NodeId(row["arrival_stop_id"].String()),
                    .headCoordinate_ = headCoordinate,
                    .trip_ = row["trip_id"].String(),
                    .departure_ = departureTime,
                    .duration_ = std::chrono::duration_cast<TSeconds>(
                        arrivalTime - departureTime)};
      });

  return result;
}

// TODO edges are now duplicated more often as they are time independent
GTFSGraph::TEdges GTFSGraph::Impl::RetrieveTransfers(
    const NodeId& tailStopId) const {
  sqlite_utils::Binding scopedBinding(
      transfersPrepStmt_,
      {{Constants::kTailStopIdParameterName, tailStopId.Id()}});

  TEdges result;
  sqlite_utils::ResultSet resultSet(transfersPrepStmt_);
  std::transform(
      resultSet.begin(), resultSet.end(), std::back_inserter(result),
      [&tailStopId](const sqlite_utils::Row& row) {
        const auto minTransferTime =
            TSeconds{row["min_transfer_time"].Integer()};
        return Edge{.from_ = tailStopId,
                    .to_ = NodeId{row["arrival_stop_id"].String()},
                    .headCoordinate_ =
                        Point{row["head_lon"].Float(), row["head_lat"].Float()},
                    .duration_ = minTransferTime};
      });
  return result;
}

Point GTFSGraph::Impl::RetrieveCoordinate(const NodeId& node) const {
  sqlite_utils::Binding binding(
      stopCoordinatesPrepStmt_,
      {{Constants::kStopCoordinatesQueryStopId, node.Id()}});

  sqlite_utils::ResultSet rs(stopCoordinatesPrepStmt_);

  auto it = rs.begin();

  if (it == rs.end()) {
    throw std::runtime_error("Failed to find coordinates of stop with id " +
                             node.Id());
  }

  const auto row = *it;
  return Point{row["stop_lon"],
               row["stop_lat"]};  // Yes, they are reversed on purpose
}

GTFSGraph::TNodes GTFSGraph::Impl::StopsAround(const Point& coordinate,
                                               const double radiusMeter) const {
  assert(radiusMeter <= 1000.0 &&
         "This is currently only optimized for small radii");
  const auto boundingBox =
      detail::GeometryHelper::SpanBoundingBox(coordinate, radiusMeter);

  sqlite_utils::Binding scopedBinding(
      reachableStopsPrepStmt_,
      {{Constants::kMinLon, boundingBox.minPoint_.lon_},
       {Constants::kMinLat, boundingBox.minPoint_.lat_},
       {Constants::kMaxLon, boundingBox.maxPoint_.lon_},
       {Constants::kMaxLat, boundingBox.maxPoint_.lat_}});

  sqlite_utils::ResultSet resultSet(reachableStopsPrepStmt_);
  TNodes nodes;

  std::transform(resultSet.begin(), resultSet.end(), std::back_inserter(nodes),
                 [](const sqlite_utils::Row& row) {
                   const Point stopCoordinate{row["stop_lon"], row["stop_lat"]};
                   return Node{.id_ = NodeId(row["stop_id"]),
                               .coordinate_ = stopCoordinate};
                 });

  return nodes;
}

GTFSGraph::TNamedNodes GTFSGraph::Impl::StopsWithName(
    const std::string& substr) const {
  auto wildcardString = "%" + substr + "%";

  sqlite_utils::Binding scopedBinding(
      stopsWithNamePrepStmt_,
      {{Constants::kStopsWithSubstrQuerySubstr, wildcardString}});

  TNamedNodes stops;
  sqlite_utils::ResultSet resultSet(stopsWithNamePrepStmt_);
  std::transform(resultSet.begin(), resultSet.end(), std::back_inserter(stops),
                 [](const sqlite_utils::Row& row) {
                   return NamedNode{.id_ = NodeId{row["stop_id"]},
                                    // TODO make this an implicit conversion?
                                    .name_ = std::string{row["stop_name"]}};
                 });
  return stops;
}

int GTFSGraph::Impl::InternalExecutionSteps() const {
  // TODO(kaihowl) localize / SSOT
  std::vector<sqlite_utils::PreparedStatement*> currentStatements = {
      &outArcsPrepStmt_,        &stopNamePrepStmt_,
      &transfersPrepStmt_,      &tripNamePrepStmt_,
      &randomNodePrepStmt_,     &stopCoordinatesPrepStmt_,
      &reachableStopsPrepStmt_, &stopsWithNamePrepStmt_,
  };

  const auto totalSteps =
      std::accumulate(currentStatements.begin(), currentStatements.end(), 0,
                      [](const auto acc, auto* stmt) {
                        auto steps = stmt->StatsVmSteps();
                        return acc + steps;
                      });
  return totalSteps;
}

std::string GTFSGraph::Describe(const NodeId& nodeId) const {
  // TODO special casing...
  if (nodeId.IsSpecial()) {
    return nodeId.Id();
  }

  sqlite_utils::Binding scopedBinding(
      pImpl_->stopNamePrepStmt_,
      {{Constants::kStationNameQueryStopId, nodeId.Id()}});

  sqlite_utils::ResultSet rs(pImpl_->stopNamePrepStmt_);

  auto it = rs.begin();

  if (it == rs.end()) {
    throw std::runtime_error("Failed to find station name");
  }

  return (*it)["stop_name"].String();
}

std::string GTFSGraph::Describe(const TTripId& tripId) const {
  sqlite_utils::Binding scopedBinding(
      pImpl_->tripNamePrepStmt_, {{Constants::kTripNameQueryTripId, tripId}});

  sqlite_utils::ResultSet rs(pImpl_->tripNamePrepStmt_);
  auto it = rs.begin();
  if (it == rs.end()) {
    throw std::runtime_error("Failed to describe");
  }

  std::stringstream ss;
  ss << (*it)["trip_headsign"].String() << " ("
     << (*it)["route_short_name"].String() << ")";
  return ss.str();
}

NodeId GTFSGraph::RandomNode() const {
  sqlite_utils::ResultSet rs(pImpl_->randomNodePrepStmt_);

  auto it = rs.begin();

  if (it == rs.end()) {
    throw std::runtime_error("Failed to find a random node");
  }

  const auto row = *it;

  return NodeId{row["stop_id"]};
}

Point GTFSGraph::StopCoordinates(const TPlace& place) const {
  using overloaded_visitor::overloaded;
  return std::visit(overloaded{[this](const graph::NodeId& nodeId) {
                                 return pImpl_->RetrieveCoordinate(nodeId);
                               },
                               [](const graph::Point& point) { return point; }},
                    place);
}  // namespace graph

Graph::TNodes GTFSGraph::StopsAround(const Point& coordinate,
                                     const double radius) const {
  return pImpl_->StopsAround(coordinate, radius);
}

GTFSGraph::TNamedNodes GTFSGraph::StopsWithName(
    const std::string& substr) const {
  return pImpl_->StopsWithName(substr);
}

int GTFSGraph::InternalExecutionSteps() const {
  return pImpl_->InternalExecutionSteps();
}
}  // namespace graph
