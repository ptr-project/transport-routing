/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "graph/types.h"  // for Point

#include <cmath>  // for cos, asin, sqrt, M_PI
#include <tuple>

namespace graph {
namespace detail {
struct GeometryHelper {
  struct BoundingBox {
    Point minPoint_;
    Point maxPoint_;
  };

  template <typename T>
  static T RadianToDegree(const T radian) {
    return radian * 180 / M_PI;
  }

  template <typename T>
  static T DegreeToRadian(const T degree) {
    return degree * M_PI / 180;
  }

  static std::tuple<double, double> LatitudeSpan(const Point& centerPoint,
                                                 const double radiusMeters) {
    const auto range = RadianToDegree(radiusMeters / kEarthRadius);
    return {centerPoint.lat_ - range, centerPoint.lat_ + range};
  }

  static std::tuple<double, double> LongitudeSpan(const Point& centerPoint,
                                                  const double radiusMeters) {
    const auto range =
        RadianToDegree(std::asin(radiusMeters / kEarthRadius) /
                       std::cos(DegreeToRadian(centerPoint.lat_)));
    return {centerPoint.lon_ - range, centerPoint.lon_ + range};
  }

  static BoundingBox SpanBoundingBox(const Point& point,
                                     const double radiusMeters) {
    const auto [minLat, maxLat] = LatitudeSpan(point, radiusMeters);
    const auto [minLon, maxLon] = LongitudeSpan(point, radiusMeters);

    return {{minLon, minLat}, {maxLon, maxLat}};
  }

  static const constexpr double kEarthRadius = 6371.009 * 1000;  // meters;
};
}  // namespace detail
}  // namespace graph
