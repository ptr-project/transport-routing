/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

namespace graph {
namespace detail {

struct Constants {
  static constexpr const char* kStopsQuery =
      "select "
      // https://www.sqlite.org/lang_select.html#resultset
      // Using min guarantees that bare columns show values of the same row
      // that also contains the min value
      " trip_id, min(departure_time) as departure_time, arrival_time, "
      " arrival_stop_id, head_lat, head_lon "
      "from stop_times_joined "
      "where "
      "  departure_stop_id = :tail_stop_id "
      "  and :arrival_at_tail_seconds <= departure_time "
      "  and departure_time <= :arrival_at_tail_seconds + 20 * 60 "
      "  and ( "
      "    (:weekday_mask & effective_weekday != 0)  "
      "      and not (has_removals "
      "               and exists (SELECT * "
      "                           FROM calendar_dates "
      "                           WHERE "
      "                            service_id = stop_times_joined.service_id "
      "                            AND date = :arrival_at_tail_date "
      "                            AND exception_type=2))"
      "    or "
      "      exists (select * "
      "              from calendar_dates "
      "              where service_id = stop_times_joined.service_id "
      "              and date = :arrival_at_tail_date and exception_type=1)"
      "  )"
      "  group by stop_times_joined.route_id;";
  // TODO There might be valid cases to keep two consecutive trips on
  // the same route. Right now this is crucial to keep the amount of edges
  // small
  // TODO make look ahead window configurable

  static constexpr const char* kTailStopIdParameterName = ":tail_stop_id";
  static constexpr const char* kArrivalAtTailSecondsParameterName =
      ":arrival_at_tail_seconds";
  static constexpr const char* kArrivalAtTailDateParameterName =
      ":arrival_at_tail_date";
  static constexpr const char* kWeekdayMaskParameterName = ":weekday_mask";

  static constexpr const char* kStationNameQuery =
      "select stop_name from stops where stop_id = :stop_id";

  static constexpr const char* kStationNameQueryStopId = ":stop_id";

  static constexpr const char* kTripNameQuery =
      "select trip_headsign, route_short_name, routes.route_id as route_id "
      "from "
      "trips inner join routes "
      "where "
      "trips.route_id = routes.route_id and trip_id = :trip_id";

  static constexpr const char* kTripNameQueryTripId = ":trip_id";

  // TODO this does not handle the different types of transfers at all
  // currently.
  static constexpr const char* kTransfersQuery =
      "select distinct "
      "  min_transfer_time as min_transfer_time, "
      "  transfers.to_stop_id as arrival_stop_id, "
      "  head_stop.stop_lat as head_lat, "
      "  head_stop.stop_lon as head_lon "
      "from transfers "
      "left join stops as head_stop "
      "where "
      "  transfer_type in (0, 2) and "
      "  from_stop_id = :tail_stop_id and "
      "  transfers.to_stop_id = head_stop.stop_id;";

  static constexpr const char* kReachableStopsQuery =
      "  select stop_id as stop_id, "
      "         stop_lat as stop_lat, "
      "         stop_lon as stop_lon "
      "  from stops "
      "  where stop_lat between :min_lat and :max_lat "
      "    and stop_lon between :min_lon and :max_lon;";
  static constexpr const char* kMinLat = ":min_lat";
  static constexpr const char* kMaxLat = ":max_lat";
  static constexpr const char* kMinLon = ":min_lon";
  static constexpr const char* kMaxLon = ":max_lon";

  static constexpr const char* kRandomNodeQuery =
      "select stop_id "
      "from stops "
      "where "
      "  stop_id in (select stop_id from stops order by random() limit 1);";

  static constexpr const char* kStopCoordinatesQuery =
      "select stop_lat, stop_lon from stops where stop_id = :stop_id";
  static constexpr const char* kStopCoordinatesQueryStopId = ":stop_id";

  static constexpr const char* kStopsWithSubstrQuery =
      "select stop_id as stop_id, stop_name as stop_name from stops where "
      "(parent_station = '' or parent_station is null) "
      "and lower(stop_name) like :substr";
  static constexpr const char* kStopsWithSubstrQuerySubstr = ":substr";
};

}  // namespace detail
}  // namespace graph
