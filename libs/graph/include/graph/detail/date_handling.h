/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "graph/types.h"

#include <date/date.h>

#include <bitset>
#include <cassert>
#include <chrono>

namespace graph {
namespace detail {

constexpr auto kNumWeekDays = 7;

using TWeekDayMask = std::bitset<kNumWeekDays>;

inline TWeekDayMask CreateWeekdayMask(const TTimePoint& timePoint) {
  TWeekDayMask result{};
  const auto weekday =
      date::year_month_weekday{date::floor<date::days>(timePoint)}.weekday();

  const auto daysTillSunday = (date::Sunday - weekday).count();
  result.set(static_cast<unsigned int>(daysTillSunday));

  return result;
}

inline unsigned char CreateWeekDayMaskChar(const TTimePoint& timePoint) {
  const auto weekdayMask = CreateWeekdayMask(timePoint);
  assert(weekdayMask.to_ulong() < std::numeric_limits<unsigned char>::max());
  return static_cast<unsigned char>(weekdayMask.to_ulong());
}

inline std::chrono::seconds GetSecondsOfDay(const TTimePoint& timePoint) {
  const auto time =
      date::make_time(timePoint - date::floor<date::days>(timePoint));
  return std::chrono::duration_cast<std::chrono::seconds>(time.to_duration());
}

inline static constexpr auto kYearShift = 10'000;
inline static constexpr auto kMonthShift = 100;

inline int GetDate(const TTimePoint& timePoint) {
  const auto date = date::year_month_day{date::floor<date::days>(timePoint)};
  const auto year = static_cast<int>(date.year());
  const auto month = static_cast<int>(static_cast<unsigned int>(date.month()));
  const auto day = static_cast<int>(static_cast<unsigned int>(date.day()));

  return year * kYearShift + month * kMonthShift + day;
}
}  // namespace detail
}  // namespace graph
