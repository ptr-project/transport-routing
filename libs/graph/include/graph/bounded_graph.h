/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "graph/graph.h"

#include <vector>

namespace graph {
/**
 * @brief A graph consisting of <n> edges on a diagonal
 */
class BoundedGraph : public Graph {
 public:
  explicit BoundedGraph(int bound);

  TEdges RetrieveTrains(const NodeId &nodeId,
                        TTimePoint arrivalTime) const override;
  TEdges RetrieveTransfers(const NodeId &nodeId) const override;

  std::string Describe(const NodeId &nodeId) const override;
  std::string Describe(const TTripId &tripId) const override;
  NodeId RandomNode() const override;
  Point StopCoordinates(const TPlace &place) const override;
  TNodes StopsAround(const Point &coordinate, double radius) const override;

  Point DepartureCoordinate() const;
  Point DestinationCoordinate() const;

 private:
  const int bound_ = 0;
  mutable TEdges edges_;
};
}  // namespace graph
