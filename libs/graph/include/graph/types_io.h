/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "graph/edge.h"
#include "graph/types.h"

#include <date/date.h>

#include <iostream>
#include <sstream>

namespace graph {

inline std::ostream& operator<<(std::ostream& stream, const Point& point) {
  return stream << "Point(" << point.lat_ << ", " << point.lon_ << ")";
}

inline std::ostream& operator<<(std::ostream& stream,
                                const TTimePoint& timepoint) {
  using date::operator<<;
  return stream << timepoint;
}

inline std::string Formatted(const TTimePoint& timepoint) {
  std::stringstream ss;
  ss << timepoint;
  return ss.str();
}

inline std::ostream& operator<<(std::ostream& stream, const TSeconds& seconds) {
  return stream << seconds.count() << " seconds";
}

template <typename TGraph>
std::string Pretty(const TGraph& graph, const Edge& edge) {
  std::stringstream ss;
  ss << "Edge(from: " << graph.Describe(edge.From())
     << " to: " << graph.Describe(edge.To())
     << " departure: " << edge.DepartureTime() << " trip: "
     << (edge.IsWalking() ? "WALK" : graph.Describe(edge.TripId()));
  return ss.str();
}

inline std::ostream& operator<<(std::ostream& stream, const NodeId& nodeId) {
  return stream << "NodeId(" << nodeId.Id() << ")";
}

inline std::ostream& operator<<(std::ostream& stream, const Edge& edge) {
  return stream << "Edge(from: " << edge.From() << ", to: " << edge.To()
                << ", departure: " << edge.DepartureTime()
                << ", duration: " << edge.TravelTime()
                << ", trip: " << edge.TripId() << ")";
}

}  // namespace graph
