/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "graph/edge.h"   // for Edge
#include "graph/node.h"   // for NodeId
#include "graph/types.h"  // for Point, TRouteId, TTripId

#include <memory>  // for unique_ptr
#include <string>  // for string
#include <vector>  // for vector

namespace graph {

class Graph {
 public:
  using TEdges = std::vector<Edge>;
  using TNodes = std::vector<Node>;

  virtual ~Graph() = default;

  [[nodiscard]] virtual TEdges RetrieveTrains(const NodeId& nodeId,
                                              TTimePoint arrivalTime) const = 0;

  [[nodiscard]] virtual TEdges RetrieveTransfers(
      const NodeId& nodeId) const = 0;

  [[nodiscard]] virtual TNodes StopsAround(const Point& coordinate,
                                           double radius) const = 0;

  [[nodiscard]] virtual std::string Describe(const NodeId& nodeId) const = 0;
  [[nodiscard]] virtual std::string Describe(const TTripId& tripId) const = 0;

  [[nodiscard]] virtual NodeId RandomNode() const = 0;

  [[nodiscard]] virtual Point StopCoordinates(const TPlace& place) const = 0;
};
}  // namespace graph
