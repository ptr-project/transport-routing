/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "graph/node.h"   // for NodeId, operator<<, operator==
#include "graph/types.h"  // for TSeconds, TTimePoint, Point, TTripId

#include <cassert>  // for assert
#include <utility>  // for move

namespace graph {

struct Edge {
 public:
  // TODO move into router
  static Edge CreateDisembark(const NodeId &station,
                              graph::Point stationCoordinate);

  // TODO extend for checking zero edges (from!=to)
  [[nodiscard]] bool IsValid() const {
    const bool hasReasonableLength = duration_ < std::chrono::hours{4};
    return from_.IsValid() && to_.IsValid() && hasReasonableLength;
  }

  [[nodiscard]] TTimePoint ArrivalTime(const TTimePoint &departAt) const {
    const bool isTimeIndependent = (departure_ == TTimePoint{});
    return isTimeIndependent ? departAt + duration_ : departure_ + duration_;
  }

  [[nodiscard]] Edge CloneForArrivalTime(const TTimePoint &arrivalTime) const {
    auto edge = *this;
    edge.departure_ = arrivalTime - duration_;
    // Ensure that we only reset an edge which did not have a valid departure
    // time before
    assert(IsWalking() || IsDisembark() || edge.departure_ == departure_);
    return edge;
  }

  template <typename TTimePoint>
  void Arrival(TTimePoint &&arrival) {
    assert(departure_ != TTimePoint{});
    duration_ = std::chrono::duration_cast<TSeconds>(
        std::forward<TTimePoint>(arrival) - departure_);
  }

  [[nodiscard]] const NodeId &From() const { return from_; }

  [[nodiscard]] const NodeId &To() const { return to_; }

  [[nodiscard]] const Point &HeadCoordinate() const { return headCoordinate_; }

  [[nodiscard]] const TTimePoint &DepartureTime() const { return departure_; }

  [[nodiscard]] const TSeconds &TravelTime() const { return duration_; }

  [[nodiscard]] const TTripId &TripId() const { return trip_; }

  [[nodiscard]] bool IsDisembark() const {
    return trip_.empty() && from_ == to_;
  }

  [[nodiscard]] bool IsWalking() const { return trip_.empty(); }

  Edge &operator+=(const Edge &rhs) {
    assert(trip_ == rhs.trip_);
    const auto finalArrivalTime = rhs.ArrivalTime(ArrivalTime(departure_));
    duration_ =
        std::chrono::duration_cast<TSeconds>(finalArrivalTime - departure_);
    to_ = rhs.to_;
    headCoordinate_ = rhs.headCoordinate_;
    return *this;
  }

  NodeId from_{};
  NodeId to_{};
  Point headCoordinate_{};

  TTripId trip_{};
  TTimePoint departure_{};
  TSeconds duration_{};
};

inline Edge Edge::CreateDisembark(const NodeId &station,
                                  Point stationCoordinate) {
  return Edge{.from_ = station,
              .to_ = station,
              .headCoordinate_ = stationCoordinate,
              .duration_ = TSeconds{0}};
}

}  // namespace graph
