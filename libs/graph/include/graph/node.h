/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "graph/types.h"

#include <cassert>     // for assert
#include <cstddef>     // for size_t
#include <functional>  // for hash
#include <string>  // for allocator, char_traits, hash, operator==, basic_string
#include <utility>  // for move
#include <variant>

namespace graph {

class NodeId {
 public:
  using TValue = std::string;
  NodeId() = default;
  // TODO The collection of constructors seems arbitrary and only tries to
  // subset string's constructors
  explicit NodeId(TValue id) : id_(std::move(id)) { assert(IsValid()); }
  explicit NodeId(const char* id) : id_{id} {}
  explicit NodeId(const unsigned char* id)
      // TODO handle unicode
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      : id_(reinterpret_cast<const char*>(id)) {
    assert(IsValid());
  }

  [[nodiscard]] TValue Id() const { return id_; }

  [[nodiscard]] bool IsValid() const { return !id_.empty(); }

  [[nodiscard]] bool IsSpecial() const {
    return IsDeparture() || IsDestination();
  }

  [[nodiscard]] bool IsDeparture() const { return id_ == DepartureId(); }

  [[nodiscard]] bool IsDestination() const { return id_ == DestinationId(); }

  static NodeId Departure() { return NodeId{DepartureId()}; }

  static NodeId Destination() { return NodeId{DestinationId()}; }

 private:
  TValue id_{};

  static const char* DepartureId() { return "Departure"; }
  static const char* DestinationId() { return "Destination"; }
};

struct Node {
  [[nodiscard]] const NodeId& Id() const { return id_; }

  [[nodiscard]] const Point& Coordinate() const { return coordinate_; }

  NodeId id_;
  Point coordinate_{};
};

struct NamedNode {
  [[nodiscard]] const NodeId& Id() const { return id_; }
  [[nodiscard]] const std::string& Name() const { return name_; }

  NodeId id_;
  std::string name_;
};

inline bool operator<(const NodeId& lhs, const NodeId& rhs) {
  return lhs.Id() < rhs.Id();
}

inline bool operator==(const NodeId& lhs, const NodeId& rhs) {
  return !(lhs < rhs) && !(rhs < lhs);
}

inline bool operator!=(const NodeId& lhs, const NodeId& rhs) {
  return !(lhs == rhs);
}

using TPlace = std::variant<NodeId, graph::Point>;
}  // namespace graph

namespace std {
template <>
struct hash<graph::NodeId> {
  using argument_type = graph::NodeId;
  using result_type = std::size_t;
  result_type operator()(const argument_type& n) const noexcept {
    return std::hash<graph::NodeId::TValue>{}(n.Id());
  }
};
}  // namespace std
