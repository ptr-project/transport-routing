/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <sqlite_utils/binding.h>             // for binding
#include <sqlite_utils/database.h>            // for Database
#include <sqlite_utils/prepared_statement.h>  // for PreparedStatement

#include <initializer_list>  // for initializer_list
#include <optional>          // for optional
#include <string>            // for string, to_string
#include <utility>           // for pair
#include <variant>           // for variant, visit

namespace graph::test::fixture {
struct TripEdge {
  std::string tripId_{"defaultTripId"};
  std::string serviceId_{"defaultServiceId"};
  std::string routeId_{"defaultRouteId"};
  int departureTime_{60};
  int arrivalTime_{100};
  std::string departureStop_{"defaultStopId"};
  std::string arrivalStop_{"defaultStopId"};
  double headLon_{0.0};
  double headLat_{0.0};
  unsigned int effectiveWeekday_{(2u << 8u) - 1};  // enabled on all weekdays
  bool hasExceptions_{false};

  static constexpr const char* kStatement =
      "INSERT INTO "
      "stop_times_joined(trip_id, service_id, route_id, "
      "departure_time, arrival_time, departure_stop_id, arrival_stop_id, "
      "head_lat, head_lon, effective_weekday, has_removals) "
      "VALUES (:trip_id, "
      ":service_id, :route_id, "
      ":departure_time, :arrival_time, :departure_stop, :arrival_stop, "
      ":head_lat, :head_lon, :effective_weekday, :has_exceptions);";

  [[nodiscard]] TripEdge Generate(const int newValue) const {
    TripEdge result = *this;
    result.tripId_ = std::to_string(newValue);
    result.serviceId_ = std::to_string(newValue);
    result.routeId_ = std::to_string(newValue);
    return result;
  }

  [[nodiscard]] auto CreateBinding(
      sqlite_utils::PreparedStatement& statement) const {
    return sqlite_utils::Binding{
        statement,
        {{":trip_id", tripId_},
         {":service_id", serviceId_},
         {":route_id", routeId_},
         {":departure_time", departureTime_},
         {":arrival_time", arrivalTime_},
         {":departure_stop", departureStop_},
         {":arrival_stop", arrivalStop_},
         {":head_lat", headLat_},
         {":head_lon", headLon_},
         {":effective_weekday", static_cast<int>(effectiveWeekday_)},
         {":has_exceptions", hasExceptions_}}

    };
  }
};

enum class CalendarDataExeption { Added = 1, Removed = 2 };

struct CalendarDateEntry {
  std::string serviceId_{"defaultServiceId"};
  int date_{0};
  CalendarDataExeption exceptionType_{CalendarDataExeption::Added};

  static constexpr const char* kStatement =
      "INSERT INTO calendar_dates(service_id, date, exception_type) VALUES "
      " (:service_id, :date, :exception_type);";

  [[nodiscard]] CalendarDateEntry Generate(const int newValue) const {
    CalendarDateEntry result = *this;
    result.date_ = newValue;
    return result;
  }

  [[nodiscard]] auto CreateBinding(
      sqlite_utils::PreparedStatement& statement) const {
    return sqlite_utils::Binding{
        statement,
        {{":service_id", serviceId_},
         {":date", date_},
         {":exception_type", static_cast<int>(exceptionType_)}}};
  }
};

struct StopEntry {
  std::string stopId_{"defaultStopId"};
  std::string stopName_{"defaultStopName"};
  double stopLat_{0.0};
  double stopLon_{0.0};
  std::optional<std::string> parentStation_{""};

  static constexpr const char* kStatement =
      "INSERT INTO stops(stop_id, stop_name, stop_lat, stop_lon, "
      "parent_station) "
      "VALUES (:stop_id, :stop_name, :stop_lat, :stop_lon, :parent_station);";

  [[nodiscard]] StopEntry Generate(const int newValue) const {
    StopEntry result = *this;
    result.stopId_ = std::to_string(newValue);
    result.stopName_ = std::to_string(newValue);
    return result;
  }

  [[nodiscard]] auto CreateBinding(
      sqlite_utils::PreparedStatement& statement) const {
    return sqlite_utils::Binding{statement,
                                 {{":stop_id", stopId_},
                                  {":stop_name", stopName_},
                                  {":stop_lat", stopLat_},
                                  {":stop_lon", stopLon_},
                                  {":parent_station", parentStation_}}};
  }
};

enum class TransferType {
  Recommended = 0,
  Timed = 1,
  MinTransferTime = 2,
  Impossible = 3
};

struct TransferEntry {
  std::string fromStopId_;
  std::string toStopId_{"defaultStopId"};
  TransferType transferType_{TransferType::Recommended};
  int minTransferTime_{0};

  static constexpr const char* kStatement =
      "INSERT INTO transfers(from_stop_id, to_stop_id, transfer_type, "
      "min_transfer_time) "
      "VALUES (:from_stop_id, :to_stop_id, :transfer_type, "
      ":min_transfer_time);";

  [[nodiscard]] TransferEntry Generate(const int newValue) const {
    TransferEntry result = *this;
    result.minTransferTime_ = newValue;
    return result;
  }

  [[nodiscard]] auto CreateBinding(
      sqlite_utils::PreparedStatement& statement) const {
    return sqlite_utils::Binding{
        statement,
        {{":from_stop_id", fromStopId_},
         {":to_stop_id", toStopId_},
         {":transfer_type", static_cast<int>(transferType_)},
         {":min_transfer_time", minTransferTime_}}};
  }
};

using TEntity =
    std::variant<TripEdge, CalendarDateEntry, StopEntry, TransferEntry>;

void DatabaseWith(sqlite_utils::Database& db,
                  const std::vector<TEntity>& entities);

sqlite_utils::Database DatabaseWith(const std::vector<TEntity>& entities);

sqlite_utils::Database PopulateDatabaseWith(std::vector<TEntity>& entities);

sqlite_utils::Database PopulateDatabaseWith(
    std::initializer_list<std::pair<std::size_t, TEntity>> quantitiesEntities);

}  // namespace graph::test::fixture
