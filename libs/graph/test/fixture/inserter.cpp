/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "graph/test/fixture/inserter.h"

#include "graph/test/fixture/empty_database.h"  // for EmptyDatabase

#include <sqlite_utils/binding.h>             // for binding
#include <sqlite_utils/database.h>            // for Database
#include <sqlite_utils/prepared_statement.h>  // for PreparedStatement
#include <sqlite_utils/transaction.h>         // for Transaction

#include <algorithm>         // for for_each, generate_n
#include <initializer_list>  // for initializer_list
#include <memory>            // for unique_ptr, make_unique
#include <random>
#include <string>  // for string, to_string
#include <typeindex>
#include <unordered_map>
#include <utility>  // for pair
#include <variant>  // for variant, visit

namespace graph::test::fixture {
sqlite_utils::Database DatabaseWith(
    const std::vector<graph::test::fixture::TEntity>& entities) {
  sqlite_utils::Database db = fixture::EmptyDatabase();

  DatabaseWith(db, entities);

  return db;
}

sqlite_utils::Database PopulateDatabaseWith(std::vector<TEntity>& entities) {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::shuffle(entities.begin(), entities.end(), gen);
  return DatabaseWith(entities);
}

sqlite_utils::Database PopulateDatabaseWith(
    const std::initializer_list<std::pair<std::size_t, TEntity>>
        quantitiesEntities) {
  std::vector<TEntity> result;
  const auto totalSize = std::accumulate(
      quantitiesEntities.begin(), quantitiesEntities.end(), 0u,
      [](const auto lhs, const auto& pair) { return lhs + pair.first; });
  result.reserve(totalSize);
  int counter = 0;
  auto generate = [&counter](const auto& templateEntity) -> TEntity {
    return templateEntity.Generate(++counter);
  };

  for (const auto& quantityEntity : quantitiesEntities) {
    std::generate_n(std::back_inserter(result), quantityEntity.first, [&] {
      return std::visit(generate, quantityEntity.second);
    });
  }
  return PopulateDatabaseWith(result);
}

void DatabaseWith(sqlite_utils::Database& db,
                  const std::vector<TEntity>& entities) {
  std::unordered_map<std::type_index,
                     std::unique_ptr<sqlite_utils::PreparedStatement>>
      preparedStatements;

  sqlite_utils::Transaction t(db);

  auto insertEntity = [&](const auto& e) {
    auto& statementPtr = preparedStatements[std::type_index(typeid(e))];
    if (!statementPtr) {
      statementPtr =
          std::make_unique<sqlite_utils::PreparedStatement>(db, e.kStatement);
    }
    const auto binding = e.CreateBinding(*statementPtr);
    const auto result = statementPtr->Step();
    (void)result;
    assert(result == SQLITE_DONE);
    statementPtr->Reset();
  };

  std::for_each(entities.begin(), entities.end(),
                [&](const auto& entity) { std::visit(insertEntity, entity); });
}

}  // namespace graph::test::fixture
