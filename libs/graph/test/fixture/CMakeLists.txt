project(graph_test_fixture)

set(INCLUDE_DIR include/graph/test/fixture)

set(SOURCES
  ${INCLUDE_DIR}/empty_database.h
  ${INCLUDE_DIR}/inserter.h
  empty_database.cpp
  inserter.cpp
)

add_library(${PROJECT_NAME} ${SOURCES})

target_link_libraries(${PROJECT_NAME}
  PUBLIC
    sqlite_utils::sqlite_utils
  PRIVATE
    input
)

target_include_directories(${PROJECT_NAME} PUBLIC include/)
