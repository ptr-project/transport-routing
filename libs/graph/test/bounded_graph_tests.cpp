/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "equals_coordinate_matcher.h"
#include "graph/bounded_graph.h"
#include "graph/distance.h"
#include "graph/types.h"
#include "graph/types_io.h"

#include <catch2/catch.hpp>

#include <sstream>
#include <utility>

// TODO required unless Point is no longer a typedef
// NOLINTNEXTLINE(readability-identifier-naming)
namespace Catch {
template <>
struct StringMaker<graph::Point> {
  // NOLINTNEXTLINE(readability-identifier-naming)
  static std::string convert(const graph::Point& value) {
    // The following line is the sole reason to keep this
    using graph::operator<<;
    std::stringstream ss;
    ss << value;
    return ss.str();
  }
};
}  // namespace Catch

namespace graph::test {

// NOLINTNEXTLINE(readability-function-size)
TEST_CASE("Zero Graph") {
  BoundedGraph zeroGraph(0);

  SECTION("Stop Around Departure and Destination") {
    const auto nodes = zeroGraph.StopsAround(Point{0.0, 0.0}, 1);
    REQUIRE(nodes.size() == 1);
    const auto& node = nodes.front();
    CHECK(node.Id() == NodeId{"0"});
    CHECK_THAT(node.Coordinate(),
               EqualsCoordinate(zeroGraph.DepartureCoordinate()));
    CHECK_THAT(node.Coordinate(),
               EqualsCoordinate(zeroGraph.DestinationCoordinate()));
  }

  const auto headNode =
      zeroGraph.StopsAround(zeroGraph.DepartureCoordinate(), 1).front();

  const Edge departureEdge{.from_ = graph::NodeId::Departure(),
                           .to_ = headNode.Id(),
                           .headCoordinate_ = headNode.Coordinate()};

  SECTION("RetrieveTrains") {
    const auto edges =
        zeroGraph.RetrieveTrains(departureEdge.From(), TTimePoint());
    CHECK(edges.empty());
  }

  SECTION("RetrieveTransfers") {
    const auto edges = zeroGraph.RetrieveTransfers(departureEdge.From());
    CHECK(edges.empty());
  }
}

// NOLINTNEXTLINE(readability-function-size)
TEST_CASE("Bounded Graph") {
  const int kBound = 100;
  BoundedGraph boundedGraph(kBound);

  SECTION("Stop Around Departure") {
    const auto nodes = boundedGraph.StopsAround(Point{0.0, 0.0}, 1);
    REQUIRE(nodes.size() == 1);
    const auto& node = nodes.front();
    CHECK(node.Id() == NodeId{"0"});
    CHECK_THAT(node.Coordinate(),
               EqualsCoordinate(boundedGraph.DepartureCoordinate()));
  }

  SECTION("Destination Arcs") {
    const auto nodes = boundedGraph.StopsAround(Point{100.0, 100.0}, 1);
    REQUIRE(nodes.size() == 1);
    const auto& node = nodes.front();
    CHECK(node.Id() == NodeId{"100"});
    CHECK_THAT(node.Coordinate(),
               EqualsCoordinate(boundedGraph.DestinationCoordinate()));
  }

  SECTION("RetrieveTrains") {
    SECTION("Departure") {
      const auto edges =
          boundedGraph.RetrieveTrains(NodeId::Departure(), TTimePoint());
      REQUIRE(edges.size() == 1);
      const auto& edge = edges.front();
      CHECK(edge.IsValid());
      CHECK(not edge.IsWalking());
      CHECK(edge.From() == NodeId::Departure());
      CHECK(edge.To() == NodeId{"1"});
      CHECK_THAT(edge.HeadCoordinate(), EqualsCoordinate({1.0, 1.0}));
    }

    SECTION("Somewhere in between") {
      const auto edges =
          boundedGraph.RetrieveTrains(NodeId{"50"}, TTimePoint());
      REQUIRE(edges.size() == 1);
      const auto& edge = edges.front();
      CHECK(edge.IsValid());
      CHECK(edge.From() == NodeId{"50"});
      CHECK(edge.To() == NodeId{"51"});
      CHECK_THAT(edge.HeadCoordinate(), EqualsCoordinate({51.0, 51.0}));
    }

    SECTION("Destination") {
      const auto edges =
          boundedGraph.RetrieveTrains(NodeId{"100"}, TTimePoint());
      CHECK(edges.empty());
    }
  }

  SECTION("RetrieveTransfers") {
    const auto edges = boundedGraph.RetrieveTransfers(NodeId{"50"});
    CHECK(edges.empty());
  }
}
}  // namespace graph::test
