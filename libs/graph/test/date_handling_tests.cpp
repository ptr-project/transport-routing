/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "graph/detail/date_handling.h"  // for CreateWeekdayMask, TWeekDayMask
#include "graph/types.h"                 // for TTimePoint

#include <date/date.h>

// We want this for prettier output of dates / durations in tests
using date::operator<<;
#include <catch2/catch.hpp>

#include <cassert>  // for assert
#include <chrono>   // for system_clock
#include <sstream>  // for stringstream, basic_istream:...
#include <string>   // for char_traits, string

namespace graph::test {

namespace {
auto Convert(const std::string& str) {
  std::stringstream ss(str);
  date::sys_seconds time;
  ss >> date::parse("%F %X", time);
  assert(!ss.fail());
  assert(!ss.bad());
  return time;
}
}  // namespace

using namespace graph::detail;
using namespace Catch::Matchers;

// NOLINTNEXTLINE(readability-function-size)
TEST_CASE("Returns correct bitmasks for weekdays") {
  using namespace date::literals;
  using namespace std::chrono_literals;

  // January 1st 2018 was a Monday
  const auto monday = date::sys_days(2018_y / 01 / 01);

  CHECK(CreateWeekdayMask(monday + date::days{0}) == TWeekDayMask("1000000"));
  CHECK(CreateWeekdayMask(monday + date::days{1}) == TWeekDayMask("0100000"));
  CHECK(CreateWeekdayMask(monday + date::days{2}) == TWeekDayMask("0010000"));
  CHECK(CreateWeekdayMask(monday + date::days{3}) == TWeekDayMask("0001000"));
  CHECK(CreateWeekdayMask(monday + date::days{4}) == TWeekDayMask("0000100"));
  CHECK(CreateWeekdayMask(monday + date::days{5}) == TWeekDayMask("0000010"));
  CHECK(CreateWeekdayMask(monday + date::days{6}) == TWeekDayMask("0000001"));
}

// NOLINTNEXTLINE(readability-function-size)
TEST_CASE("Returns correct seconds since start of day") {
  using namespace std::chrono_literals;

  CHECK(GetSecondsOfDay(Convert("2018-01-01 00:00:00")) == 0s);
  CHECK(GetSecondsOfDay(Convert("2018-01-01 00:00:01")) == 1s);
  CHECK(GetSecondsOfDay(Convert("2018-01-01 06:00:00")) == 6 * 60 * 60s);
  CHECK(GetSecondsOfDay(Convert("2018-01-01 12:00:00")) == 12 * 60 * 60s);
  CHECK(GetSecondsOfDay(Convert("2018-01-01 18:00:00")) == 18 * 60 * 60s);
  CHECK(GetSecondsOfDay(Convert("2018-01-01 23:59:59")) ==
        (24 * 60 * 60s - 1s));
}

// NOLINTNEXTLINE(readability-function-size)
TEST_CASE("Returns correct date as fake int") {
  CHECK(GetDate(Convert("2018-01-01 00:00:00")) == 20180101);
  CHECK(GetDate(Convert("1990-12-20 00:00:00")) == 19901220);
  CHECK(GetDate(Convert("2032-06-03 00:00:00")) == 20320603);
}
}  // namespace graph::test
