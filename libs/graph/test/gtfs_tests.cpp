/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "graph/gtfs_graph.h"
#include "graph/test/fixture/empty_database.h"
#include "graph/test/fixture/inserter.h"
#include "graph/types.h"
#include "graph/types_io.h"

#include <sqlite_utils/binding.h>
#include <sqlite_utils/database.h>
#include <sqlite_utils/prepared_statement.h>

using graph::operator<<;
// Order is of the essence here to get proper pretty prints
#include "matchers/equals_coordinate_matcher.h"

#include <date/date.h>

#include <catch2/catch.hpp>

#include <algorithm>
#include <initializer_list>
#include <optional>
#include <set>
#include <string>
#include <utility>
#include <variant>
#include <vector>

namespace graph::test {
using namespace fixture;

TEST_CASE("GTFS graph truly empty") {
  CHECK_THROWS_WITH(GTFSGraph(":memory:"),
                    Catch::Contains("Failed to prepare"));
}

TEST_CASE("GTFS graph in-memory") {
  fixture::EmptyDatabase db;
  CHECK_NOTHROW(GTFSGraph{std::move(db)});
}

// NOLINTNEXTLINE(readability-function-size)
TEST_CASE("Round trip edge properties") {
  const auto& insertAndSelectEdge = [](const TripEdge& edge) {
    GTFSGraph graph{DatabaseWith({edge})};

    const auto& edges = graph.RetrieveTrains(
        graph::NodeId(edge.departureStop_),
        graph::TTimePoint(graph::TSeconds(edge.departureTime_)));

    REQUIRE(edges.size() == 1);
    REQUIRE(edges.front().IsValid());

    CHECK_THAT(graph.InternalExecutionSteps(), Catch::WithinRel(84, 0.01));

    return edges.front();
  };

  SECTION("Departure Station") {
    TripEdge e;

    e.departureStop_ = "stop_0";
    CHECK(insertAndSelectEdge(e).From() == NodeId{"stop_0"});
  }

  SECTION("Arrival Station") {
    TripEdge e;
    e.arrivalStop_ = "myArrivalStop";
    CHECK(insertAndSelectEdge(e).To() == NodeId{"myArrivalStop"});
  }

  SECTION("Trip Id") {
    TripEdge e;
    e.tripId_ = "myTripId";
    CHECK(insertAndSelectEdge(e).TripId() == "myTripId");
  }

  SECTION("Arrival Time") {
    TripEdge e;
    e.arrivalTime_ = 180;
    CHECK(insertAndSelectEdge(e).ArrivalTime(TTimePoint{}) ==
          TTimePoint{TSeconds{180}});
  }

  SECTION("Head Coordinate") {
    TripEdge e;
    e.headLat_ = 44.0;
    e.headLon_ = 66.0;
    CHECK_THAT(insertAndSelectEdge(e).HeadCoordinate(),
               EqualsCoordinate(Point{66.0, 44.0}));
  }
}

// NOLINTNEXTLINE(readability-function-size)
TEST_CASE("Only return edges for correct departure station") {
  TripEdge e1;
  e1.departureTime_ = 60;
  e1.departureStop_ = "stop_0";

  TripEdge e2;
  e2.departureTime_ = 60;
  e2.departureStop_ = "stop_1";

  GTFSGraph g{DatabaseWith({e1, e2})};

  SECTION("From first station") {
    const auto& edges =
        g.RetrieveTrains(NodeId{"stop_0"}, TTimePoint{TSeconds{60}});
    REQUIRE(edges.size() == 1);
    CHECK(edges.front().From() == NodeId{"stop_0"});
    CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(85, 0.01));
  }

  SECTION("From second station") {
    const auto& edges =
        g.RetrieveTrains(NodeId{"stop_1"}, TTimePoint{TSeconds{60}});
    REQUIRE(edges.size() == 1);
    CHECK(edges.front().From() == NodeId{"stop_1"});
    CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(84, 0.01));
  }
}

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Train timings") {
  GIVEN("Two trips for the same route, same stop but different departures") {
    TripEdge e1;
    e1.departureStop_ = "stop_0";
    e1.routeId_ = "route_0";
    e1.departureTime_ = 60;

    TripEdge e2;
    e2.departureStop_ = "stop_0";
    e2.routeId_ = "route_0";
    e2.departureTime_ = 90;

    GTFSGraph g{DatabaseWith({e1, e2})};

    WHEN("Retrieving outgoing trains for the earlier departure") {
      // Shortly before and directly on the departure time
      const int departureTime = GENERATE(values({50, 60}));
      CAPTURE(departureTime);

      const auto& edges = g.RetrieveTrains(NodeId{"stop_0"},
                                           TTimePoint{TSeconds{departureTime}});

      THEN("Only the earlier departure should be listed") {
        REQUIRE(edges.size() == 1);
        CHECK(edges.front().DepartureTime() == TTimePoint{TSeconds{60}});
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(113, 0.01));
      }
    }

    WHEN("Retrieving outgoing trains for the later departure") {
      // shortly before and directly on the departure time
      const int departureTime = GENERATE(values({80, 90}));
      CAPTURE(departureTime);

      const auto& edges = g.RetrieveTrains(NodeId{"stop_0"},
                                           TTimePoint{TSeconds{departureTime}});

      THEN("Only the later departure should be listed") {
        REQUIRE(edges.size() == 1);
        CHECK(edges.front().DepartureTime() == TTimePoint{TSeconds{90}});
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(84, 0.01));
      }
    }
  }
}

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Calendar service exclusions") {
  using namespace date::literals;

  GIVEN("A trip valid on Sundays") {
    TripEdge e;
    e.serviceId_ = "service_1";
    e.departureStop_ = "stop_0";
    e.departureTime_ = 0;
    e.effectiveWeekday_ = 1;  // Sunday as encoded by "0b0000001"

    WHEN("Including a non-sunday date as an exception") {
      CalendarDateEntry cal;
      cal.date_ = 20180507;  // a Monday
      cal.serviceId_ = "service_1";
      cal.exceptionType_ = CalendarDataExeption::Added;

      GTFSGraph g{DatabaseWith({e, cal})};

      THEN("Querying on that day should find the trip") {
        CHECK(not g.RetrieveTrains(NodeId{"stop_0"},
                                   date::sys_days{2018_y / 05 / 07})
                      .empty());
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(98, 0.01));
      }

      THEN("Querying on a different Monday should not find the trip") {
        CHECK(
            g.RetrieveTrains(NodeId{"stop_0"}, date::sys_days{2018_y / 05 / 14})
                .empty());
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(49, 0.01));
      }

      THEN("Querying on any Sunday should find the trip") {
        CHECK(
            not g.RetrieveTrains(NodeId{"stop_0"},
                                 date::sys_days{2018_y / 05 / date::Sunday[1]})
                    .empty());
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(84, 0.01));
      }
    }

    WHEN("Excluding a specific Sunday") {
      e.hasExceptions_ = true;

      CalendarDateEntry cal;
      cal.date_ = 20180506;
      cal.serviceId_ = "service_1";
      cal.exceptionType_ = CalendarDataExeption::Removed;

      GTFSGraph g{DatabaseWith({e, cal})};

      THEN("Querying on that Sunday should not find a trip") {
        CHECK(
            g.RetrieveTrains(NodeId{"stop_0"}, date::sys_days{2018_y / 05 / 06})
                .empty());
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(71, 0.01));
      }

      THEN("Querying on a different Sunday should find the trip") {
        CHECK(not g.RetrieveTrains(NodeId{"stop_0"},
                                   date::sys_days{2018_y / 05 / 13})
                      .empty());
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(95, 0.01));
      }
    }
  }

  GIVEN("A trip that has no valid weekday") {
    TripEdge e;
    e.serviceId_ = "service_0";
    e.departureStop_ = "stop_0";
    e.departureTime_ = 0;
    e.effectiveWeekday_ = 0;  // No valid weekday

    WHEN("Including a specific date for that trip") {
      CalendarDateEntry cal;
      cal.serviceId_ = "service_0";
      cal.date_ = 20180607;
      cal.exceptionType_ = CalendarDataExeption::Added;

      GTFSGraph g{DatabaseWith({e, cal})};

      THEN("Querying on that specific date should find the trip") {
        CHECK(not g.RetrieveTrains(NodeId{"stop_0"},
                                   date::sys_days{2018_y / 06 / 07})
                      .empty());
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(98, 0.01));
      }

      THEN("Querying on a different date should not find the trip") {
        CHECK(
            g.RetrieveTrains(NodeId{"stop_0"}, date::sys_days{2018_y / 05 / 06})
                .empty());
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(49, 0.01));
      }
    }
  }
}

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Weekday handling") {
  using namespace date::literals;

  GIVEN("A trip valid on a specific weekday") {
    const auto daysFromMonday =
        static_cast<unsigned int>(GENERATE(range(0, 6)));
    CAPTURE(daysFromMonday);

    TripEdge e;
    e.departureStop_ = "stop_0";
    e.effectiveWeekday_ = 1u << (6u - daysFromMonday);

    GTFSGraph g{DatabaseWith({e})};

    WHEN("Querying on the specific weekday") {
      const auto& edges =
          g.RetrieveTrains(NodeId{"stop_0"}, date::sys_days{2018_y / 05 / 07} +
                                                 date::days{daysFromMonday});

      THEN("The trip should be found") {
        CHECK(not edges.empty());
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(84, 0.01));
      }
    }

    WHEN("Querying on a different weekday") {
      const auto& edges = g.RetrieveTrains(
          NodeId{"stop_0"},
          date::sys_days{2018_y / 05 / 07} + date::days{daysFromMonday + 1});

      THEN("The trip trip should not be found") {
        CHECK(edges.empty());
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(49, 0.01));
      }
    }
  }
}

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Simple Transfers") {
  GIVEN("Two stops and a transfer between them") {
    StopEntry stop1;
    stop1.stopId_ = "firstStation";
    stop1.stopLat_ = 1.0;
    stop1.stopLon_ = 2.0;

    StopEntry stop2;
    stop2.stopId_ = "secondStation";
    stop2.stopLat_ = 3.0;
    stop2.stopLon_ = 4.0;

    TransferEntry transfer;
    transfer.fromStopId_ = "firstStation";
    transfer.toStopId_ = "secondStation";
    transfer.minTransferTime_ = 123;

    GTFSGraph g{DatabaseWith({stop1, stop2, transfer})};

    WHEN("Retrieving transfers for the first station") {
      const auto& transfers = g.RetrieveTransfers(NodeId{"firstStation"});
      REQUIRE(transfers.size() == 1);

      const auto& edge = transfers.front();

      THEN("The edge should match the database entries") {
        CHECK(edge.From() == NodeId{"firstStation"});
        CHECK(edge.To() == NodeId{"secondStation"});
        CHECK_THAT(edge.HeadCoordinate(), EqualsCoordinate(Point{4.0, 3.0}));
        CHECK(edge.TravelTime() == TSeconds{123});
      }

      THEN("The edge should be marked as walking") {
        CHECK(edge.IsWalking());
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(33, 0.01));
      }
    }

    WHEN("Retrieving transfers for the second station") {
      const auto& transfers = g.RetrieveTransfers(NodeId{"secondStation"});

      THEN("There should be no transfers") {
        CHECK(transfers.empty());
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(15, 0.01));
      }
    }

    WHEN("Adding an additional transfer to a third station") {
      StopEntry stop3;
      stop3.stopId_ = "thirdStation";

      TransferEntry transfer2;
      transfer2.fromStopId_ = "firstStation";
      transfer2.toStopId_ = "thirdStation";

      GTFSGraph g2{DatabaseWith({stop1, stop2, stop3, transfer, transfer2})};

      THEN("We find two transfers with different head stations") {
        const auto& transfers = g2.RetrieveTransfers(NodeId{"firstStation"});
        REQUIRE(transfers.size() == 2);
        std::set expectedHeadStations{NodeId{"secondStation"},
                                      NodeId{"thirdStation"}};
        std::set actualStations{transfers.front().To(), transfers.back().To()};
        CHECK(expectedHeadStations == actualStations);
        CHECK_THAT(g2.InternalExecutionSteps(), Catch::WithinRel(53, 0.01));
      }
    }
  }
}

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Reachable stops") {
  GIVEN("Two stops, one reachable, one not") {
    StopEntry uKlosterstr;
    uKlosterstr.stopId_ = "00001";
    uKlosterstr.stopLat_ = 52.518030;
    uKlosterstr.stopLon_ = 13.411993;

    StopEntry uMaerkischesMuseum;
    uMaerkischesMuseum.stopId_ = "00002";
    uMaerkischesMuseum.stopLat_ = 52.512682;
    uMaerkischesMuseum.stopLon_ = 13.411115;

    GTFSGraph g{DatabaseWith({uKlosterstr, uMaerkischesMuseum})};

    WHEN("Retrieving stops around Molkenstr") {
      Point molkenStr{.lon_ = 13.409245, .lat_ = 52.516866};

      THEN("In 250m I find a single stop") {
        const auto stops = g.StopsAround(molkenStr, 250);
        REQUIRE(stops.size() == 1);
        CHECK(stops.front().Id() == NodeId{"00001"});
        CHECK_THAT(stops.front().Coordinate(),
                   EqualsCoordinate(Point{.lon_ = uKlosterstr.stopLon_,
                                          .lat_ = uKlosterstr.stopLat_}));
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(34, 0.01));
      }

      THEN("In 500m I find two stops") {
        const auto stops = g.StopsAround(molkenStr, 500);
        REQUIRE(stops.size() == 2);
        std::vector<NodeId> stopIds;
        std::transform(stops.begin(), stops.end(), std::back_inserter(stopIds),
                       [](const Node& stop) { return stop.Id(); });
        CHECK_THAT(stopIds, Catch::UnorderedEquals(
                                std::vector{NodeId{"00001"}, NodeId{"00002"}}));
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(41, 0.01));
      }
    }
  }
}

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Stops with name") {
  GIVEN("Two stops 'Station A' and 'Station B'") {
    StopEntry stationA{.stopId_ = "A", .stopName_ = "Station A"};
    StopEntry stationB{.stopId_ = "B", .stopName_ = "Station B"};

    GTFSGraph g(DatabaseWith({stationA, stationB}));
    WHEN("Retrieving stops by the name 'Station A'") {
      const auto stops = g.StopsWithName("Station A");

      THEN("Only a single result should be returned") {
        REQUIRE(stops.size() == 1);
        CHECK(stops.front().Id() == graph::NodeId("A"));
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(25, 0.01));
      }
    }
  }

  GIVEN("A stop and substations named 'The Test Station'") {
    StopEntry parent{.stopId_ = "parent", .stopName_ = "The Test Station"};
    StopEntry substation{.stopId_ = "sub",
                         .stopName_ = "The Test Station",
                         .parentStation_ = "parent"};

    GTFSGraph g{DatabaseWith({parent, substation})};
    WHEN("Retrieving stops by the name 'test'") {
      const auto stops = g.StopsWithName("test");
      THEN("Only the parent station should be returned") {
        REQUIRE(stops.size() == 1);
        CHECK(stops.front().Id() == graph::NodeId("parent"));
        CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(23, 0.01));
      }
    }
  }

  GIVEN("A stop with a null parent station field") {
    StopEntry stop{.stopName_ = "Main Station", .parentStation_ = std::nullopt};

    GTFSGraph g{DatabaseWith({stop})};

    WHEN("Retrieving stops") {
      const auto stops = g.StopsWithName("main");
      REQUIRE(stops.size() == 1);
      CHECK_THAT(g.InternalExecutionSteps(), Catch::WithinRel(20, 0.01));
    }
  }
}
}  // namespace graph::test
