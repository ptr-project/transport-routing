/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "graph/distance.h"
#include "graph/types.h"
#include "graph/types_io.h"

#include <catch2/catch.hpp>

#include <sstream>

namespace graph {
namespace test {
class EqualsCoordinate : public Catch::MatcherBase<Point> {
 public:
  explicit EqualsCoordinate(const Point& coordinate)
      : comparisonCoordinate_{coordinate} {}

  std::string describe() const override {
    std::stringstream ss;
    ss << "to be equal (have zero distance) to " << comparisonCoordinate_;
    return ss.str();
  }

  bool match(const Point& coordinate) const override {
    return Catch::WithinULP(0.0, 1).match(
        graph::Distance(coordinate, comparisonCoordinate_).value());
  }

 private:
  Point comparisonCoordinate_;
};
}  // namespace test
}  // namespace graph
