/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "graph/bounded_graph.h"

#include "graph/edge.h"
#include "graph/node.h"
#include "overloaded_visitor/overloaded.h"

#include <boost/lexical_cast.hpp>

#include <string>
#include <variant>

namespace graph {

namespace {
const TTripId kFakeTrip{"FAKETRIP"};
}

BoundedGraph::BoundedGraph(int bound) : bound_{bound} {}

Graph::TEdges BoundedGraph::RetrieveTrains(const NodeId &nodeId,
                                           TTimePoint arrivalTime) const {
  const auto fromNode =
      nodeId == NodeId::Departure() ? 0 : boost::lexical_cast<int>(nodeId.Id());

  if (fromNode >= bound_) {
    return {};
  }

  auto toId = [](const int id) { return NodeId{std::to_string(id)}; };
  auto fakeCoordinate = [](const double id) { return Point{id, id}; };

  return {Edge{.from_ = nodeId,
               .to_ = toId(fromNode + 1),
               .headCoordinate_ = fakeCoordinate(fromNode + 1),
               .trip_ = kFakeTrip,
               .departure_ = arrivalTime,
               .duration_ = TMinutes{1}}};
}

Graph::TEdges BoundedGraph::RetrieveTransfers(const NodeId & /*nodeId*/) const {
  return {};
}

std::string BoundedGraph::Describe(const NodeId & /*nodeId*/) const {
  return "FAKENODE";
}

std::string BoundedGraph::Describe(const TTripId & /*tripId*/) const {
  return kFakeTrip;
}

NodeId BoundedGraph::RandomNode() const { return NodeId{"RANDOM"}; }

Point BoundedGraph::StopCoordinates(const TPlace &place) const {
  using overloaded_visitor::overloaded;
  return std::visit(overloaded{[](const graph::NodeId &nodeId) {
                                 const auto coordinate =
                                     boost::lexical_cast<double>(nodeId.Id());
                                 return Point{coordinate, coordinate};
                               },
                               [](const graph::Point &point) { return point; }},
                    place);
}

Graph::TNodes BoundedGraph::StopsAround(const Point &coordinate,
                                        const double /*radius*/) const {
  if (static_cast<int>(coordinate.lat_) != static_cast<int>(coordinate.lon_)) {
    // Only support coordinate directly on the diagonal
    return {};
  }

  const auto nodeIndex = static_cast<int>(coordinate.lat_);

  if (nodeIndex > bound_) {
    return {};
  }

  const auto nodeCoordinate = static_cast<double>(nodeIndex);
  return {Node{.id_ = NodeId{std::to_string(nodeIndex)},
               .coordinate_ = Point{nodeCoordinate, nodeCoordinate}}};
}

// NOLINTNEXTLINE(readability-convert-member-functions-to-static
Point BoundedGraph::DepartureCoordinate() const { return Point{0.0, 0.0}; }

Point BoundedGraph::DestinationCoordinate() const {
  const auto diagonalPoint = static_cast<double>(bound_);
  return {diagonalPoint, diagonalPoint};
}

}  // namespace graph
