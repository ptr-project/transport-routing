/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "env_helper/mappath.h"  // for GetMapPathFromEnv
#include "graph/gtfs_graph.h"    // for GraphDatabase
#include "graph/node.h"          // for NodeId
#include "graph/types.h"         // for TTimePoint, THours, TMinutes
#include "router/router.h"       // for Router, Router::Args

#include <chrono>       // for milliseconds, operator+, dura...
#include <iostream>     // for operator<<, basic_ostream, endl
#include <ratio>        // for ratio
#include <string>       // for operator<<
#include <type_traits>  // for __success_type<>::type, enabl...
#include <vector>       // for vector

using namespace router;
using namespace graph;

using TGraph = graph::GTFSGraph;

const auto kAdlershof = "060193002004";
const auto kPankow = "060130002642";
const auto kGruenau = "060186001812";
// TODO add again after fixing the long walking path issue
// const auto kSchlossBabelsberg = "270000044201";
const auto kTreptowerPark = "060190001571";
const auto kWittenbergPlatz = "070201033104";

const auto kDepartureTime =
    TTimePoint(THours(13) + TMinutes(40) + TSeconds(1552089600));

namespace {
void CalculateRouteAndPrint(const Graph& graph, const NodeId& departure,
                            const NodeId& destination) {
  std::cout << "------\n"
            << graph.Describe(departure) << " --> "
            << graph.Describe(destination) << std::endl;

  const auto start = std::chrono::system_clock::now();
  Router router(Router::WithArgs(graph)
                    .Departure(departure)
                    .Destination(destination)
                    .DepartureTime(TTimePoint(kDepartureTime)));

  const auto route = router.FindRoute();

  const auto end = std::chrono::system_clock::now();
  std::cout << "Took "
            << std::chrono::duration_cast<std::chrono::milliseconds>(end -
                                                                     start)
                   .count()
            << " ms" << std::endl;

  if (!route) {
    std::cout << "!!!Route not found!!!" << std::endl;
  } else {
    router.PrintRoute();
  }
}
}  // namespace

int main() {
  const TGraph graph{env_helper::GetMapPathFromEnv()};

  const std::vector<const char*> stations{kAdlershof, kPankow, kGruenau,
                                          kTreptowerPark, kWittenbergPlatz};

  for (std::size_t depIndex = 0; depIndex < stations.size(); ++depIndex) {
    for (std::size_t destIndex = depIndex + 1; destIndex < stations.size();
         ++destIndex) {
      CalculateRouteAndPrint(graph, NodeId(stations[depIndex]),
                             NodeId(stations[destIndex]));
    }
  }
}
