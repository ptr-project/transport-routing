/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <cassert>
#include <functional>
#include <numeric>
#include <utility>
#include <vector>

namespace cache {

struct DefaultTag {};

template <typename Tag = DefaultTag>
class CacheIndex {
 public:
  explicit CacheIndex(std::size_t index) : index_{index} {}

  // TODO maybe explicit after all?
  // NOLINTNEXTLINE(google-explicit-constructor)
  operator std::size_t() const { return index_; }

 private:
  std::size_t index_;
};

template <typename T>
CacheIndex<> MakeCacheIndex(const T& index) {
  return CacheIndex<>{index};
}

template <typename T, typename Tag = DefaultTag>
class Cache {
 public:
  using key_type = CacheIndex<Tag>;
  using IndexType = key_type;
  using IndexList = std::vector<IndexType>;

  template <typename... Arg>
  IndexType Emplace(Arg&&... arg) {
    elements_.emplace_back(std::forward<Arg>(arg)...);
    return IndexType{elements_.size() - 1};
  }

  template <typename Container>
  IndexList Insert(Container&& container) {
    // TODO review code complexity
    const auto oldSize = elements_.size();
    elements_.insert(elements_.end(), container.begin(), container.end());
    IndexList newIndexes(elements_.size() - oldSize, IndexType{1});
    if (!newIndexes.empty()) {
      newIndexes.front() = IndexType{oldSize};
    }
    std::partial_sum(newIndexes.begin(), newIndexes.end(), newIndexes.begin(),
                     [](const IndexType a, const IndexType b) {
                       return IndexType{static_cast<std::size_t>(a) +
                                        static_cast<std::size_t>(b)};
                     });
    return newIndexes;
  }

  T operator[](const IndexType& index) const {
    assert(index < elements_.size());
    return elements_[index];
  }

 private:
  std::vector<T> elements_;
};
}  // namespace cache

namespace std {
template <typename Tag>
struct hash<cache::CacheIndex<Tag>> {
  std::size_t operator()(
      const cache::CacheIndex<Tag>& cacheIndex) const noexcept {
    return hash<std::size_t>{}(cacheIndex);
  }
};
}  // namespace std
