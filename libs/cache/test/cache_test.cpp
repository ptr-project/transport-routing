/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cache/cache.h"

#include <catch2/catch.hpp>

#include <type_traits>
#include <vector>

namespace {
struct DummyValue {
  // NOLINTNEXTLINE(google-explicit-constructor)
  DummyValue(const int val) : val_{val} {}
  int val_{};
};

struct MyIndexTag {};
using MyCache = cache::Cache<DummyValue, MyIndexTag>;

struct MyOtherTag {};
using MyOtherCache = cache::Cache<DummyValue, MyOtherTag>;

static_assert(
    !std::is_convertible<MyCache::key_type, MyOtherCache::key_type>::value,
    "Should not be able to mix up two key types");

}  // namespace

using namespace Catch;

namespace cache::test {

using TIndex = CacheIndex<>;

// NOLINTNEXTLINE(readability-function-size)
TEST_CASE("Default Cache") {
  Cache<DummyValue> cache;

  SECTION("Inserting single value") {
    const auto index = cache.Emplace(1);
    CHECK(index == 0);
  }

  SECTION("Inserting empty container") {
    const auto indexes = cache.Insert(std::vector<DummyValue>{});
    CHECK(indexes.empty());
  }

  SECTION("Inserting container with values") {
    const auto indexes = cache.Insert(std::vector<DummyValue>{1, 2, 3});
    CHECK_THAT(indexes,
               Equals(std::vector<TIndex>{TIndex{0}, TIndex{1}, TIndex{2}}));
  }

  const auto firstIndex = cache.Emplace(1);

  SECTION("Inserting container after first value") {
    const auto indexes = cache.Insert(std::vector<DummyValue>{1, 2, 3});
    CHECK_THAT(indexes,
               Equals(std::vector<TIndex>{TIndex{1}, TIndex{2}, TIndex{3}}));
  }

  SECTION("Retrieve first element") { CHECK(cache[firstIndex].val_ == 1); }

  SECTION("Retrieve first element from const") {
    const auto& constCache = cache;
    CHECK(constCache[firstIndex].val_ == 1);
  }
}

// NOLINTNEXTLINE(readability-function-size)
TEST_CASE("Special IndexType Cache") {
  MyCache cache;

  SECTION("Insert single value") {
    // Not using auto to ensure type
    const CacheIndex<MyIndexTag> index = cache.Emplace(1);
    CHECK(index == 0);
  }

  SECTION("Inserting multiple values") {
    const std::vector<CacheIndex<MyIndexTag>> indexes =
        cache.Insert(std::vector<DummyValue>{1, 2, 3});
    CHECK(indexes.size() == 3);
  }
}

}  // namespace cache::test
