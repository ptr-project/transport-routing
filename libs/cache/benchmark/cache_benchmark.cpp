/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cache/cache.h"
#include "graph/edge.h"

#include <catch2/catch.hpp>
#include <catch2_extended/custom_macros.h>

#include <cstdint>
#include <string>
#include <vector>

namespace cache::test {

namespace {
void FillWithFakeEdges(std::vector<graph::Edge>& v, const std::int64_t count) {
  const graph::Edge fakeEdge(
      graph::Edge{.from_ = graph::NodeId{"0"},
                  .to_ = graph::NodeId{"1"},
                  .headCoordinate_ = graph::Point{0.0, 0.0}});
  v.insert(v.end(), static_cast<std::size_t>(count), fakeEdge);
}

std::vector<graph::Edge> FakeEdges(const std::int64_t count) {
  std::vector<graph::Edge> edges;
  FillWithFakeEdges(edges, count);
  return edges;
}
}  // namespace

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Cache benchmark") {
  SECTION("Repeated benchmark") {
    auto numberEdges = GENERATE(2, 4, 8, 16, 32);

    BENCHMARK_STABLE_STATISTICS("Copy vector into cache")
    (auto meter) {
      cache::Cache<graph::Edge> cache;
      meter.measure([&] { cache.Insert(FakeEdges(numberEdges)); });
      meter["numberEdges"] = numberEdges;
      return cache;
    };

    BENCHMARK_STABLE_STATISTICS("Copy vector into vector")
    (auto meter) {
      std::vector<graph::Edge> edges;
      meter.measure([&] {
        const auto newEdges = FakeEdges(numberEdges);
        edges.insert(edges.end(), newEdges.begin(), newEdges.end());
      });
      meter["numberEdges"] = numberEdges;
      return edges;
    };

    BENCHMARK_STABLE_STATISTICS("Create elements directly in target vector")
    (auto meter) {
      std::vector<graph::Edge> edges;
      meter.measure([&] { FillWithFakeEdges(edges, numberEdges); });
      meter["numberEdges"] = numberEdges;
      return edges;
    };
  }
}

}  // namespace cache::test
