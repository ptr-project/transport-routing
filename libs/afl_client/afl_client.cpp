/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "env_helper/mappath.h"
#include "graph/gtfs_graph.h"
#include "graph/types.h"
#include "graph/types_io.h"
#include "router/router.h"

#include <chrono>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <utility>

std::tuple<graph::Point, graph::Point, int> GetInput(std::istream& inStream) {
  graph::Point from{};
  graph::Point to{};

  int departureTime = 0;

  inStream >> from.lat_;
  inStream >> from.lon_;
  inStream >> to.lat_;
  inStream >> to.lon_;
  inStream >> departureTime;

  if (!inStream) {
    throw std::runtime_error("Could not construct route request");
  }

  return {from, to, departureTime};
}

constexpr auto kPopCountBudget = 10'000;

void CalculateRoute(std::istream& inStream) {
  const auto [from, to, departureTime] = GetInput(inStream);

  std::cout << "Searching route from " << from << " to " << to << " at "
            << departureTime << std::endl;

  graph::GTFSGraph g{env_helper::GetMapPathFromEnv()};

  router::Router r(
      router::Router::WithArgs(g).Departure(from).Destination(to).DepartureTime(
          graph::TTimePoint(std::chrono::seconds{departureTime})));

  if (r.FindRoute(kPopCountBudget)) {
    r.PrintRoute();
  } else {
    std::cerr << "No route found" << std::endl;
  }
}

int main() {
  try {
    CalculateRoute(std::cin);
  } catch (const std::runtime_error& error) {
    std::cerr << error.what() << std::endl;
    return 1;
  }

  return 0;
}
