/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "env_helper/mappath.h"
#include "graph/gtfs_graph.h"
#include "graph/types.h"
#include "router/router.h"
#include "router/router_metrics.h"

#include <catch2/catch.hpp>
#include <catch2_extended/custom_macros.h>

#include <chrono>
#include <string>
#include <utility>
#include <vector>

namespace router::test {
using namespace graph;
namespace {
const auto kDepartureTime =
    TTimePoint(THours(13) + TMinutes(40) + TSeconds(946684800));
}  // namespace

// NOLINTNEXTLINE(readability-function-size)
SCENARIO("Router Benchmarks") {
  SECTION("Reference Routes") {
    static const std::vector<std::string> kStations = {
        "900000193002",  // Adlershof
        "900000190001",  // Treptower Park
        "900000130002",  // Pankow
        "900000186001",  // Gruenau
        "900000056101"   // Wittenberg Platz
    };
    const auto fromNode = NodeId(GENERATE(from_range(kStations)));
    const auto toNode = NodeId(GENERATE(from_range(kStations)));

    graph::GTFSGraph graph{env_helper::GetMapPathFromEnv()};

    BENCHMARK_STATISTICS("Reference Route")
    (auto meter) {
      router::RouterMetrics metrics;
      const auto routerArgs = Router::WithArgs(graph)
                                  .Departure(fromNode)
                                  .Destination(toNode)
                                  .RouterMetrics(metrics)
                                  .DepartureTime(kDepartureTime);

      TMaybeRoute route;
      meter.measure([&] {
        Router r(routerArgs);
        route = r.FindRoute();
      });

      meter["fromName"] = graph.Describe(fromNode);
      meter["toName"] = graph.Describe(toNode);
      meter["numEdges"] = metrics.numEdges_;
      meter["popCount"] = metrics.popCounter_;
      meter["travelTimeSeconds"] =
          std::chrono::duration_cast<std::chrono::seconds>(metrics.travelTime_)
              .count();
      meter["visitedEdges"] = metrics.visitedEdges_;
      meter["numEdges"] = metrics.numEdges_;
      meter["numRouteChanges"] = metrics.numRouteChanges_;

      return route;
    };
  }

  SECTION("Abnormal Route") {
    const auto adlershof = graph::NodeId{"900000193002"};
    const auto schlossBabelsberg = graph::NodeId{"900000230048"};

    graph::GTFSGraph graph{env_helper::GetMapPathFromEnv()};
    const auto routerArgs = Router::WithArgs(graph)
                                .Departure(adlershof)
                                .Destination(schlossBabelsberg)
                                .DepartureTime(kDepartureTime);

    BENCHMARK("Adlershof to Schloss Babelsberg") {
      Router r(routerArgs);
      return r.FindRoute();
    };
  }

  SECTION("A-Star Tuning") {
    const auto aStarFactor = GENERATE(values({0, 1, 2, 4, 6, 8, 16}));

    const auto spandauFarNorthWest = graph::NodeId("900000029101");
    const auto adlershofFarSouthEast = graph::NodeId("900000193002");

    graph::GTFSGraph graph{env_helper::GetMapPathFromEnv()};
    const auto routerArgs = Router::WithArgs(graph)
                                .Departure(spandauFarNorthWest)
                                .Destination(adlershofFarSouthEast)
                                .DepartureTime(kDepartureTime)
                                .AStarFactor(aStarFactor);

    BENCHMARK_STATISTICS("Routing with custom a-star factor")(auto meter) {
      Router r(routerArgs);
      meter.measure([&] { return r.FindRoute(); });
      meter["aStarFactor"] = aStarFactor;
    };
  }
}
}  // namespace router::test
