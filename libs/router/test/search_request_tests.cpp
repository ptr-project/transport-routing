/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <catch2/catch.hpp>

#include <graph/bounded_graph.h>
#include <router/router.h>

using namespace Catch;

namespace router::test {

// NOLINTNEXTLINE(readability-function-size)
TEST_CASE("Router should fail for impossible requests") {
  graph::BoundedGraph emptyGraph(0);

  SECTION("Unmatchable departure") {
    const auto args = router::Router::WithArgs(emptyGraph)
                          .Departure(graph::Point{5.0, 5.0})
                          .Destination(graph::Point{0.0, 0.0});

    REQUIRE_THROWS_WITH(router::Router(args).FindRoute(),
                        Contains("departure"));
  }

  SECTION("Unmatchable destination") {
    const auto args = router::Router::WithArgs(emptyGraph)
                          .Departure(graph::Point{0.0, 0.0})
                          .Destination(graph::Point{5.0, 5.0});

    REQUIRE_THROWS_WITH(router::Router(args).FindRoute(),
                        Contains("destination"));
  }
}
}  // namespace router::test
