/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "equals_coordinate_matcher.h"
#include "graph/edge.h"
#include "graph/node.h"
#include "graph/types_io.h"
#include "router/detail/finalization.h"

#include <catch2/catch.hpp>

#include <chrono>
#include <vector>

namespace router::detail::test {

using namespace graph;
using namespace std::chrono_literals;

// NOLINTNEXTLINE(readability-function-size)
TEST_CASE("route finalization") {
  SECTION("two edges with pause inbetween") {
    std::vector a = {Edge{.from_ = NodeId{"1"},
                          .to_ = NodeId{"2"},
                          .headCoordinate_ = Point{2.0, 2.0},
                          .trip_ = TTripId{"FirstTrip"},
                          .departure_ = TTimePoint{5s},
                          .duration_ = TSeconds{2}},
                     Edge{.from_ = NodeId{"2"},
                          .to_ = NodeId{"3"},
                          .headCoordinate_ = Point{3.0, 3.0},
                          .trip_ = TTripId{"FirstTrip"},
                          .departure_ = TTimePoint{10s},
                          .duration_ = TSeconds{5}}};

    const auto conflatedEdges = ConflateTripsAlongRoute(a);
    CHECK(conflatedEdges.size() == 1);

    const auto& edge = conflatedEdges.front();

    CHECK(edge.From() == NodeId{"1"});
    CHECK(edge.To() == NodeId{"3"});

    CHECK_THAT(edge.HeadCoordinate(),
               graph::test::EqualsCoordinate(Point{3.0, 3.0}));

    CHECK(edge.DepartureTime() == TTimePoint{5s});
    using graph::operator<<;
    std::stringstream ss;
    ss << edge.DepartureTime();
    CHECK(edge.ArrivalTime(edge.DepartureTime()) == TTimePoint{15s});
  }

  SECTION("two edges of different trips") {
    std::vector a = {Edge{.from_ = NodeId{"1"},
                          .to_ = NodeId{"2"},
                          .headCoordinate_ = Point{2.0, 2.0},
                          .trip_ = TTripId{"FirstTrip"}},
                     Edge{.from_ = NodeId{"2"},
                          .to_ = NodeId{"3"},
                          .headCoordinate_ = Point{3.0, 3.0},
                          .trip_ = TTripId{"SecondTrip"}}};

    CHECK(ConflateTripsAlongRoute(a).size() == 2);
  }
}
}  // namespace router::detail::test
