/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "graph/bounded_graph.h"
#include "router/router.h"
#include "router/router_metrics.h"

#include <catch2/catch.hpp>

namespace router::test {

using namespace Catch;

// NOLINTNEXTLINE(readability-function-size)
TEST_CASE("Limit route search with budget") {
  graph::BoundedGraph g(10);

  router::RouterMetrics metrics{};

  Router r(Router::WithArgs(g)
               .Departure(g.DepartureCoordinate())
               .Destination(g.DestinationCoordinate())
               .DepartureTime(graph::TTimePoint{graph::TSeconds{42}})
               .RouterMetrics(metrics));

  SECTION("No Budget") {
    CHECK_THROWS_WITH(r.FindRoute(0), Contains("Budget"));
    CHECK(metrics.popCounter_ == 0);
  }

  SECTION("Budget too small to find route") {
    CHECK_THROWS_WITH(r.FindRoute(5), Contains("Budget"));
    CHECK(metrics.popCounter_ == 5);
  }

  // Needs one more to pop off Departure -> (0,0) label
  SECTION("Budget big enough to find route") {
    const auto maybeRoute = r.FindRoute(11);
    CHECK(metrics.popCounter_ == 11);
    CHECK(maybeRoute.has_value());
  }
}

// NOLINTNEXTLINE(readability-function-size)
TEST_CASE("Limit route search without route metrics should also work") {
  graph::BoundedGraph g(10);

  Router r(Router::WithArgs(g)
               .Departure(g.DepartureCoordinate())
               .Destination(g.DestinationCoordinate())
               .DepartureTime(graph::TTimePoint{graph::TSeconds{42}}));

  CHECK_THROWS_WITH(r.FindRoute(5), Contains("Budget"));
}
}  // namespace router::test
