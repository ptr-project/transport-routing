/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "router/detail/cached_graph.h"

#include "graph/distance.h"  // for Distance
#include "graph/graph.h"     // for Graph

#include <boost/range/join.hpp>  // for joined_range, join

#include <algorithm>  // for transform
#include <cassert>    // for assert
#include <iterator>   // for back_inserter

namespace router::detail {
CachedGraph::CachedGraph(const graph::Graph& graph) : graph_{graph} {}

CachedGraph::Edge router::detail::CachedGraph::GetEdge(
    const CachedGraph::EdgeId& edgeId) const {
  return edgeCache_[edgeId];
}

CachedGraph::EdgeIds CachedGraph::OutArcs(
    const graph::Edge& incomingEdge,
    const CachedGraph::TimePoint arrivalTime) const {
  assert(incomingEdge.IsValid());

  std::vector<graph::Edge> outEdges;
  if (incomingEdge.IsDisembark()) {
    // TODO skip self-referential edges
    outEdges = graph_.RetrieveTransfers(incomingEdge.To());
  } else {
    outEdges = graph_.RetrieveTrains(incomingEdge.To(), arrivalTime);
    if (!incomingEdge.IsWalking()) {
      outEdges.emplace_back(graph::Edge::CreateDisembark(
          incomingEdge.To(), incomingEdge.HeadCoordinate()));
      assert(outEdges.back().IsDisembark());
    }
  }

  return edgeCache_.Insert(outEdges);
}

template <CachedGraph::AnchorDirection direction>
CachedGraph::EdgeIds CachedGraph::RetrieveAnchorArcs(
    const graph::Point& coordinate, const double radiusMeter) const {
  const auto makeAnchor = [&coordinate](const graph::Node& reachableNode) {
    switch (direction) {
      case AnchorDirection::Departure:
        return Edge{.from_ = graph::NodeId::Departure(),
                    .to_ = reachableNode.Id(),
                    .headCoordinate_ = reachableNode.Coordinate()};
      case AnchorDirection::Destination:
        return Edge{.from_ = reachableNode.Id(),
                    .to_ = graph::NodeId::Destination(),
                    .headCoordinate_ = coordinate};
    }
  };

  const auto& source = graph_.StopsAround(coordinate, radiusMeter);
  std::vector<graph::Edge> newEdges;

  std::transform(
      source.begin(), source.end(), std::back_inserter(newEdges),
      [&coordinate, &makeAnchor](const graph::Node& node) {
        auto anchor = makeAnchor(node);
        const auto distance = graph::Distance(coordinate, node.Coordinate());
        // TODO magic numbers
        anchor.duration_ =
            graph::TSeconds{static_cast<int>(distance / 3000.0 * 60 * 60)};
        return anchor;
      });

  return edgeCache_.Insert(newEdges);
}  // namespace graph

CachedGraph::EdgeIds CachedGraph::DepartureArcs(
    const graph::Point& coordinate, const double radiusMeters) const {
  return RetrieveAnchorArcs<AnchorDirection::Departure>(coordinate,
                                                        radiusMeters);
}

CachedGraph::EdgeIds CachedGraph::DestinationArcs(
    const graph::Point& coordinate, double radiusMeters) const {
  return RetrieveAnchorArcs<AnchorDirection::Destination>(coordinate,
                                                          radiusMeters);
}

const graph::Graph& CachedGraph::UnderlyingGraph() const { return graph_; }
}  // namespace router::detail
