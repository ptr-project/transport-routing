/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "router/router.h"

#include "graph/edge.h"
#include "graph/types.h"
#include "graph/types_io.h"
#include "overloaded_visitor/overloaded.h"
#include "router/detail/algorithm.h"
#include "router/detail/cost_model.h"
#include "router/detail/route_functions.h"
#include "router/detail/search_goal.h"
#include "router/detail/search_request.h"
#include "router/router_metrics.h"

#include <cassert>
#include <iostream>
#include <limits>
#include <optional>
#include <stdexcept>
#include <utility>  // for std::in_place_t
#include <vector>

namespace router {

using namespace graph;
using namespace detail;

namespace {
const double kBootstrapFuzzyRadius = 100.0;  // meters

struct TDeparture {};
struct TDestination {};

template <typename TGoalType>
SearchGoal MakeSearchGoal(const RouterData &routerData,
                          const CachedGraph &cachedGraph) {
  using overloaded_visitor::overloaded;

  SearchGoal goal{};

  const auto memberSelector = overloaded{
      [](TDeparture /*unused*/) { return &RouterData::departure_; },
      [](TDestination /*unused*/) { return &RouterData::destination_; }};

  goal.coordinate_ = cachedGraph.UnderlyingGraph().StopCoordinates(
      (routerData.*memberSelector(TGoalType{})));

  const auto methodSelector = overloaded{
      [](TDeparture /*unused*/) { return &CachedGraph::DepartureArcs; },
      [](TDestination /*unused*/) { return &CachedGraph::DestinationArcs; }};

  goal.edges_ = (cachedGraph.*methodSelector(TGoalType{}))(
      goal.coordinate_, kBootstrapFuzzyRadius);

  return goal;
}

SearchRequest MakeSearchRequest(const RouterData &routerData,
                                const CachedGraph &cachedGraph) {
  SearchGoal departure = MakeSearchGoal<TDeparture>(routerData, cachedGraph);
  if (departure.edges_.empty()) {
    throw std::runtime_error("No stations in reach of departure found");
  }
  departure.time_ = routerData.departureTime_;

  SearchGoal destination =
      MakeSearchGoal<TDestination>(routerData, cachedGraph);
  if (destination.edges_.empty()) {
    throw std::runtime_error("No stations in reach of destination found");
  }

  return {CostModel(destination.coordinate_, routerData.aStarFactor_),
          std::move(departure), std::move(destination)};
}

void FillSummaryMetrics(const RouterData &routerData, const TMaybeRoute &route,
                        RouterMetrics &metrics) {
  if (!route) {
    assert(false);
    return;
  }
  metrics.numEdges_ = route->size();
  metrics.travelTime_ = TravelTime(routerData.departureTime_, route);
  metrics.numRouteChanges_ = NumRouteChanges(route);
}

}  // namespace

struct Router::Impl {
  explicit Impl(const Graph &graph) : cachedGraph_(graph) {}

  CachedGraph cachedGraph_;
};

Router::Router(Args args)
    : RouterData(std::move(args)),
      impl_{std::make_unique<Router::Impl>(GetGraph())} {}

Router::~Router() = default;

void Router::PrintRoute() const { PrintRoute(std::cout); }

void Router::PrintRoute(std::ostream &stream) const {
  using graph::TTripId;

  if (!route_ || route_->empty()) {
    assert(false);
    return;
  }

  for (const auto &edge : *route_) {
    if (edge.IsWalking()) {
      continue;
    }

    stream << edge.DepartureTime() << " " << GetGraph().Describe(edge.From())
           << "\n";

    stream << " -- " << GetGraph().Describe(edge.TripId()) << " --> ";

    stream << "\n";

    stream << edge.ArrivalTime(edge.DepartureTime()) << " "
           << GetGraph().Describe(edge.To()) << "\n";
  }

  stream << "Total travel time: "
         << std::chrono::duration_cast<std::chrono::minutes>(
                TravelTime(departureTime_, route_))
                .count()
         << " minutes" << std::endl;
}

std::string Router::RouteToString() const {
  std::ostringstream sstream;
  PrintRoute(sstream);
  return sstream.str();
}

TMaybeRoute Router::FindRoute() {
  return FindRoute(std::numeric_limits<int>::max());
}

TMaybeRoute Router::FindRoute(int maxPopCount) {
  assert(!route_);

  const auto searchRequest = MakeSearchRequest(*this, impl_->cachedGraph_);

  auto algoArgs = Algorithm::Create(impl_->cachedGraph_, searchRequest);

  std::optional<SearchObserver> maybeSearchObserver;
  if (searchObserver_) {
    maybeSearchObserver.emplace(impl_->cachedGraph_,
                                searchRequest.departure_.coordinate_);
    algoArgs.SetSearchObserver(*maybeSearchObserver);
  }

  if (metrics_) {
    algoArgs.SetRouteMetrics(*metrics_);
  }

  algoArgs.SetPopCountBudget(maxPopCount);

  route_ = Algorithm(algoArgs).Search();

  if (metrics_) {
    FillSummaryMetrics(*this, route_, *metrics_);
  }

  if (maybeSearchObserver) {
    maybeSearchObserver->Write(
        *searchObserver_);  // TODO confusing with both names
  }

  return route_;
}
}  // namespace router
