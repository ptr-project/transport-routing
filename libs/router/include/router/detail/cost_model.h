/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "graph/distance.h"       // for Distance
#include "graph/node.h"           // for NodeId
#include "graph/types.h"          // for TTripId, Point, TSeconds, TTimePoint
#include "router/detail/label.h"  // for Label, Label::TDuration
#include "router/router.h"        // for Router, Router::Graph

#include <units.h>

#include <chrono>  // for duration, duration<>::__no_overflow<>::type, duration_cast, operator*, oper...
#include <cmath>   // for floor
#include <cstddef>  // for size_t
#include <iosfwd>   // for string
#include <ratio>    // for ratio
#include <string>   // for operator!=

namespace router {
namespace detail {
class CostModel final {
  using Point = graph::Point;
  using Edge = graph::Edge;
  using TSeconds = graph::TSeconds;
  using TTimePoint = graph::TTimePoint;

 public:
  CostModel(const Point& destination, int aStarFactor);

  struct LabelComparison;

  static std::string SettleEdgeString(const Edge& edge);

  [[nodiscard]] TSeconds EstimateTravelTimeFrom(const Point& from) const;

  [[nodiscard]] static TSeconds Cost(const Edge& incomingEdge,
                                     const Edge& outgoingEdge,
                                     const TTimePoint& earliestDepartureTime);

 private:
  Point destination_;
  int aStarFactor_{0};
};

inline CostModel::CostModel(const Point& destination, const int aStarFactor)
    : destination_(destination), aStarFactor_(aStarFactor) {}

inline CostModel::TSeconds CostModel::Cost(
    const Edge& incomingEdge, const Edge& outgoingEdge,
    const TTimePoint& earliestDepartureTime) {
  const auto costWithWaitTime = std::chrono::duration_cast<TSeconds>(
      outgoingEdge.ArrivalTime(earliestDepartureTime) - earliestDepartureTime);

  const bool isLeavingTrain = !incomingEdge.IsWalking() &&
                              incomingEdge.TripId() != outgoingEdge.TripId();

  const auto transferCost =
      isLeavingTrain ? std::chrono::minutes{20} : std::chrono::minutes::zero();

  return costWithWaitTime + transferCost;
}

inline CostModel::TSeconds CostModel::EstimateTravelTimeFrom(
    const Point& from) const {
  const auto distance = graph::Distance(from, destination_);

  using namespace units::literals;
  const std::chrono::nanoseconds travelTime = distance / (80_km / 1_hr);
  return aStarFactor_ * std::chrono::duration_cast<TSeconds>(travelTime);
}

struct CostModel::LabelComparison {
  bool operator()(const Label& lhs, const Label& rhs) const {
    return lhs.TotalCost() > rhs.TotalCost();
  }
};

// \note Does not consider walking edges: No walking edge incoming to a
// node that was already reached via walking is considered again
inline std::string CostModel::SettleEdgeString(const Edge& edge) {
  return edge.To().Id() + "/" + edge.TripId();
}
}  // namespace detail
}  // namespace router
