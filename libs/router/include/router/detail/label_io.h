/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "graph/types.h"     // for operator<<, TTimePoint
#include "graph/types_io.h"  // for Pretty
#include "label.h"
#include "router/detail/cached_graph.h"  // for CachedGraph

#include <iostream>  // for operator<<, basic_ostream, ostream, char_traits, stringstream, string
#include <sstream>  // for stringstream

namespace router {
namespace detail {

inline std::string Pretty(const CachedGraph &graph, const Label &label) {
  std::stringstream ss;
  ss << "Label("
     << Pretty(graph.UnderlyingGraph(), graph.GetEdge(label.edgeId_)) << ")";
  return ss.str();
}

inline std::ostream &operator<<(std::ostream &stream, const Label &label) {
  using graph::operator<<;
  return stream << "Label(edgeId: " << label.edgeId_
                << ", arrivalTime: " << label.arrivalTime_ << ")";
}
}  // namespace detail
}  // namespace router
