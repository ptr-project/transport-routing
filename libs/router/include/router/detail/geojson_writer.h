/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "overloaded_visitor/overloaded.h"
#include "router/detail/geojson_types.h"

#include <nlohmann/json.hpp>

#include <string>
#include <variant>
#include <vector>

namespace router {
namespace detail {
// NOLINTNEXTLINE(readability-identifier-naming)
static void to_json(nlohmann::json& j, const Timeframe& timeframe) {
  j["start"] = timeframe.start_;
  j["end"] = timeframe.end_;
}

// NOLINTNEXTLINE(readability-identifier-naming)
static void to_json(nlohmann::json& j, const Point& point) {
  j.push_back(point.lon_);
  j.push_back(point.lat_);
}

// NOLINTNEXTLINE(readability-identifier-naming)
static void to_json(nlohmann::json& j, const Line& line) {
  j.push_back(nlohmann::json(line.from_));
  j.push_back(nlohmann::json(line.to_));
}

// NOLINTNEXTLINE(readability-identifier-naming)
static void to_json(nlohmann::json& j, const Geometry& feature) {  // TODO
  using overloaded_visitor::overloaded;
  j = std::visit(overloaded{[](const Point& point) {
                              nlohmann::json geoJson;
                              geoJson["type"] = "Point";
                              geoJson["coordinates"] = point;
                              return geoJson;
                            },
                            [](const Line& line) {
                              nlohmann::json geoJson;
                              geoJson["type"] = "LineString";
                              geoJson["coordinates"] = line;
                              return geoJson;
                            }},
                 feature);
}

// NOLINTNEXTLINE(readability-identifier-naming)
static void to_json(nlohmann::json& j, const TimeboxedFeature& feature) {
  j["type"] = "Feature";

  auto& properties = j["properties"];
  properties["timeframe"] = feature.timeframe_;
  properties["cost"] = feature.cost_;
  properties["aStarEstimate"] = feature.aStarEstimate_;
  properties["arrival"] = feature.arrival_;
  properties["departure"] = feature.departure_;
  properties["traveltime"] = feature.travelTime_;
  properties["tripId"] = feature.tripId_;
  properties["tripShortName"] = feature.tripShortName_;

  j["geometry"] = feature.geometry_;
}

class GeoJsonWriter {
 public:
  using json = nlohmann::json;

  void AppendCollection(const FeatureCollection& collection);

  [[nodiscard]] std::string Serialize() const;

 private:
  nlohmann::json json_;
};

inline void GeoJsonWriter::AppendCollection(
    const FeatureCollection& collection) {
  nlohmann::json c;
  c["type"] = "FeatureCollection";
  c["properties"]["name"] = collection.name_;
  c["properties"]["color"] = collection.color_;
  c["features"] = collection.features_;
  json_.push_back(c);
}

inline std::string GeoJsonWriter::Serialize() const { return json_.dump(); }

}  // namespace detail
}  // namespace router
