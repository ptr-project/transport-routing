/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "graph/graph.h"
#include "graph/types.h"
#include "router/router.h"

#include <algorithm>
#include <cassert>
#include <chrono>
#include <cstddef>

namespace router {
// TODO move to a translation unit
inline std::chrono::seconds TravelTime(const graph::TTimePoint departureTime,
                                       const TMaybeRoute &route) {
  using namespace graph;

  if (!route) {
    assert(false);
    return TSeconds{0};
  }

  TTimePoint arrivalTime = departureTime;
  for (const auto &edge : *route) {
    assert(arrivalTime <= edge.ArrivalTime(arrivalTime));
    arrivalTime = edge.ArrivalTime(arrivalTime);
  }

  return std::chrono::duration_cast<std::chrono::seconds>(arrivalTime -
                                                          departureTime);
}

// TODO move to a translation unit
inline long NumRouteChanges(const TMaybeRoute &route) {
  assert(route && !route->empty());

  const auto trips = std::count_if(
      route->begin(), route->end(),
      [](auto &&edge) { return !edge.IsWalking() && !edge.IsDisembark(); });

  return std::max(0L, trips - 1);
}
}  // namespace router
