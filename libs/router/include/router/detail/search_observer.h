/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "router/detail/cached_graph.h"  // for CachedGraph, CachedGraph::EdgeId
#include "router/detail/geojson_types.h"  // for FeatureCollection, Timeframe (ptr only)
#include "router/detail/label.h"

#include <functional>     // for std::hash
#include <ostream>        // for ostream
#include <tuple>          // for tuple
#include <unordered_map>  // for unordered_map

namespace graph {
struct Point;
}

namespace router {
namespace detail {

// NOTE This only works as the mapping from edge ids to labels is injective
// Until labels are stored upon finalization there is no index for the label
struct LabelHash {
  std::size_t operator()(const Label& label) const noexcept {
    return std::hash<decltype(label.edgeId_)>{}(label.edgeId_);
  }
};

struct LabelComparison {
  bool operator()(const Label& lhs, const Label& rhs) const noexcept {
    return lhs.edgeId_ == rhs.edgeId_;
  }
};

class SearchObserver final {
 public:
  using TEdgeId = CachedGraph::EdgeId;

  SearchObserver(const router::detail::CachedGraph& graph,
                 const graph::Point& departure)
      : graph_(graph), departure_{departure} {}

  void OnLabelPop();

  void OnLabeled(const Label& label);

  void OnSettled(const Label& label);

  void OnDiscarded(const Label& label);

  void OnFinalArc(const Label& label);

  void Finalize();

  void Write(std::ostream& stream) const;

 private:
  using TLabelCollection =
      std::unordered_map<Label, Timeframe, LabelHash, LabelComparison>;

  void MarkEndOfLabel(const Label& label);
  static void ExtendEndToMaxStart(TLabelCollection& collection);

  using LineExtent = std::tuple<graph::Point, graph::Point>;
  [[nodiscard]] LineExtent GetLineExtent(const TEdgeId& edgeId) const;

  FeatureCollection MakeCollection(const char* name,
                                   const TLabelCollection& labelCollection,
                                   const char* color) const;

  const router::detail::CachedGraph& graph_;
  const graph::Point& departure_;
  bool isFinalized_{false};
  int step_{0};
  TLabelCollection labeled_;
  TLabelCollection settled_;
  TLabelCollection finalRoute_;
};

}  // namespace detail
}  // namespace router
