/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <string>
#include <variant>
#include <vector>

namespace router {
namespace detail {

struct Timeframe {
  int start_{-1};
  int end_{-1};
};

struct Point {
  double lon_;
  double lat_;
};

struct Line {
  Point from_;
  Point to_;
};

using Geometry = std::variant<Point, Line>;

struct TimeboxedFeature {
  Timeframe timeframe_;
  Geometry geometry_;
  long cost_{0};
  long aStarEstimate_{0};
  std::string departure_;
  std::string arrival_;
  long travelTime_{0};
  std::string tripId_;
  std::string tripShortName_;
};

struct FeatureCollection {
  std::string name_;
  std::string color_;
  std::vector<TimeboxedFeature> features_;
};

}  // namespace detail
}  // namespace router
