/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "cache/cache.h"  // for Cache
#include "graph/edge.h"   // for Edge
#include "graph/graph.h"  // for Graph
#include "graph/node.h"   // for NodeId
#include "graph/types.h"  // for Point (ptr only), TTimePoint

#include <vector>  // for vector

namespace router {
namespace detail {
class CachedGraph {
  struct EdgeTag {};

 public:
  using Edge = graph::Edge;
  using NodeId = graph::NodeId;
  using TimePoint = graph::TTimePoint;

  using Cache = cache::Cache<Edge, EdgeTag>;
  using EdgeId = Cache::key_type;
  using EdgeIds = std::vector<EdgeId>;

  enum class AnchorDirection { Departure, Destination };

  explicit CachedGraph(const graph::Graph& graph);

  // Must return value. Otherwise, cache might be reallocated beneath....
  // TODO would be better with a slot map?
  Edge GetEdge(const EdgeId& edgeId) const;

  EdgeIds OutArcs(const graph::Edge& incomingEdge, TimePoint arrivalTime) const;
  EdgeIds DepartureArcs(const graph::Point& coordinate,
                        double radiusMeters) const;
  EdgeIds DestinationArcs(const graph::Point& coordinate,
                          double radiusMeters) const;

  const graph::Graph& UnderlyingGraph() const;

 private:
  template <AnchorDirection direction>
  CachedGraph::EdgeIds RetrieveAnchorArcs(const graph::Point& coordinate,
                                          double radiusMeter) const;

  const graph::Graph& graph_;
  mutable Cache edgeCache_;
};

}  // namespace detail
}  // namespace router
