/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

// TODO get rid of cache dependency??
#include "cache/cache.h"
#include "graph/edge.h"                  // for operator<<, Edge
#include "graph/types.h"                 // for operator<<, TTimePoint
#include "router/detail/cached_graph.h"  // for CachedGraph

#include <chrono>   // for operator+, seconds
#include <cstddef>  // for size_t

namespace router {
namespace detail {
struct Label;
struct LabelTag {};
using TLabelStore = cache::Cache<Label, LabelTag>;
using TLabelIndex = TLabelStore::IndexType;

struct Label {
  using EdgeId = CachedGraph::EdgeId;
  using TTimePoint = graph::TTimePoint;
  using TDuration = std::chrono::seconds;

  Label(const EdgeId edgeId, const TTimePoint arrivalTime)
      : edgeId_{edgeId}, arrivalTime_{arrivalTime} {}

  [[nodiscard]] TTimePoint EstimateArrivalAtDestination() const {
    return arrivalTime_ + aStarEstimate_;
  }

  [[nodiscard]] TDuration TotalCost() const { return cost_ + aStarEstimate_; }

  [[nodiscard]] bool IsDeparture() const {
    return predecessor_ == kInvalidPredecessor;
  }

  [[nodiscard]] bool IsValid() const { return arrivalTime_ != TTimePoint(); }

  EdgeId edgeId_;

  TLabelIndex predecessor_{kInvalidPredecessor};

  // TODO rename. This is arrivalAtTail, right??
  TTimePoint arrivalTime_;
  TDuration cost_{0};
  TDuration aStarEstimate_{0};

  static const std::size_t kInvalidPredecessor = static_cast<std::size_t>(-1);
};

}  // namespace detail
}  // namespace router
