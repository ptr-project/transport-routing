/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "graph/edge.h"                     // for Edge
#include "graph/node.h"                     // for NodeId, operator<, operator==
#include "graph/types.h"                    // for TTimePoint, Point, TSeconds
#include "router/detail/cached_graph.h"     // for CachedGraph
#include "router/detail/finalization.h"     // for ConflateTripsAlongRoute
#include "router/detail/label.h"            // for Label, TLabelIndex
#include "router/detail/search_observer.h"  // for SearchObserver
#include "router/detail/search_request.h"   // for SearchRequest
#include "router/router.h"                  // for Router, TRoute, Router::Graph
#include "router/router_metrics.h"          // for RouterMetrics

#include <algorithm>   // for is_sorted, lower_bound, reverse, sort
#include <cassert>     // for assert
#include <cstddef>     // for size_t
#include <functional>  // for reference_wrapper
#include <iosfwd>      // for string
#include <optional>
#include <queue>          // for priority_queue
#include <stdexcept>      // for runtime_error
#include <tuple>          // for tie, ignore, tuple
#include <type_traits>    // for move, forward
#include <unordered_set>  // for unordered_set
#include <utility>        // for pair
#include <vector>         // for vector

namespace router {
namespace detail {

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define OPTIONAL_METHOD(opt, method_name)                    \
  template <typename... Args>                                \
  void method_name(Args &&... args) {                        \
    if (opt) {                                               \
      (opt)->get().method_name(std::forward<Args>(args)...); \
    }                                                        \
  }

using MaybeSearchObserverRef =
    std::optional<std::reference_wrapper<detail::SearchObserver>>;

struct MaybeSearchObserverProxy {
  MaybeSearchObserverRef searchObserver_;

  MaybeSearchObserverProxy() = default;

  explicit MaybeSearchObserverProxy(MaybeSearchObserverRef searchObserver)
      : searchObserver_{searchObserver} {}

  OPTIONAL_METHOD(searchObserver_, OnLabeled)
  OPTIONAL_METHOD(searchObserver_, OnLabelPop)
  OPTIONAL_METHOD(searchObserver_, OnSettled)
  OPTIONAL_METHOD(searchObserver_, OnDiscarded)
  OPTIONAL_METHOD(searchObserver_, OnFinalArc)
  OPTIONAL_METHOD(searchObserver_, Finalize)
};

using MaybeMetricsRef = std::optional<std::reference_wrapper<RouterMetrics>>;

struct MaybeRouterMetricsProxy {
  MaybeMetricsRef routerMetrics_;

  MaybeRouterMetricsProxy() = default;
  explicit MaybeRouterMetricsProxy(MaybeMetricsRef metrics)
      : routerMetrics_{metrics} {}

  OPTIONAL_METHOD(routerMetrics_, IncreasePopCounter)
  OPTIONAL_METHOD(routerMetrics_, IncreaseVisitedEdges)
};

class AlgorithmData {
 protected:
  AlgorithmData(const CachedGraph &graph, SearchRequest searchRequest)
      : graph_{graph}, searchRequest_{std::move(searchRequest)} {}

  const CachedGraph &graph_;
  SearchRequest searchRequest_;
  MaybeRouterMetricsProxy metrics_;
  int popCountBudget_{0};
  MaybeSearchObserverProxy searchObserver_;
};

class Algorithm final : private AlgorithmData {
  using TSettled = std::unordered_set<std::string>;
  using TLabelQueue = std::priority_queue<Label, std::vector<Label>,
                                          CostModel::LabelComparison>;
  using Point = graph::Point;
  using TSeconds = graph::TSeconds;
  using TTimePoint = graph::TTimePoint;
  using EdgeId = CachedGraph::EdgeId;
  using EdgeIds = CachedGraph::EdgeIds;
  using Edge = graph::Edge;
  using NodeId = graph::NodeId;
  using TMaybeEdgeId = std::optional<EdgeId>;

  struct EdgeDepartureComp;

  class Args;

 public:
  explicit Algorithm(Args args);
  static Args Create(const CachedGraph &graph, SearchRequest searchRequest);

  TMaybeRoute Search();

 private:
  void Bootstrap();

  //! \returns true iff the edge is newly settled, false otherwise
  bool Settle(const Edge &edge);

  bool IsSettled(const Edge &edge);

  template <typename Label>
  TLabelIndex StoreLabel(Label &&label);

  [[nodiscard]] Label CreateDepartureLabel(const EdgeId &outEdgeId) const;

  [[nodiscard]] Label CreateLabel(const Label &prevLabel,
                                  const TLabelIndex &prevLabelIndex,
                                  const EdgeId &outEdgeId) const;

  TRoute AssembleRoute(const TLabelIndex &finalLabelIndex);

  TRoute BackTraceRoute(const TLabelIndex &finalLabelIndex);

  TMaybeEdgeId KnownEdgeToDestination(const NodeId &from);

  Label PopMinLabel();

  void VisitEdge(const Edge &incomingEdge, const Label &minLabel,
                 TLabelIndex minLabelIndex);

  TRoute FinalizeRoute(const EdgeId &destinationEdgeId, const Label &minLabel,
                       TLabelIndex minLabelIndex);

  void TryIncreasePopCount();

  TLabelQueue queue_;
  TSettled settled_;
  TLabelStore labels_;
  // As long as pop count is optional in RouteMetrics,
  // we have to duplicate this to make sure that we
  // can always implement the budget constraints
  int popCount_{0};
};

struct Algorithm::EdgeDepartureComp {
  using Graph = CachedGraph;
  using NodeId = graph::NodeId;

  explicit EdgeDepartureComp(const Graph &graph) : graph_{graph} {}

  bool operator()(const EdgeId &lhs, const EdgeId &rhs) const {
    return operator()(lhs, DepartureOfEdge(rhs));
  }

  bool operator()(const EdgeId &lhs, const NodeId &rhs) const {
    return DepartureOfEdge(lhs) < rhs;
  }

  bool operator()(const NodeId &lhs, const EdgeId &rhs) const {
    return lhs < DepartureOfEdge(rhs);
  }

 private:
  [[nodiscard]] NodeId DepartureOfEdge(const EdgeId &edgeId) const {
    return graph_.GetEdge(edgeId).From();
  }

  const Graph &graph_;
};

class Algorithm::Args : public AlgorithmData {
 public:
  Args(const CachedGraph &graph, SearchRequest searchRequest)
      : AlgorithmData(graph, std::move(searchRequest)) {}

  Args &SetSearchObserver(SearchObserver &searchObserver) {
    searchObserver_ = MaybeSearchObserverProxy(searchObserver);
    return *this;
  }

  Args &SetRouteMetrics(RouterMetrics &metrics) {
    metrics_ = MaybeRouterMetricsProxy(metrics);
    return *this;
  }

  Args &SetPopCountBudget(int popCountBudget) {
    popCountBudget_ = popCountBudget;
    return *this;
  }
};

inline Algorithm::Args Algorithm::Create(const CachedGraph &graph,
                                         SearchRequest searchRequest) {
  return Args(graph, std::move(searchRequest));
}

inline Algorithm::Algorithm(Algorithm::Args args)
    : AlgorithmData(std::move(args)) {
  auto &destinationEdges = searchRequest_.destination_.edges_;
  std::sort(destinationEdges.begin(), destinationEdges.end(),
            EdgeDepartureComp(graph_));
  Bootstrap();
}

inline void Algorithm::Bootstrap() {
  assert(queue_.empty());

  // TODO this does not handle timed edges very well...
  // Or does it?
  for (const auto &edgeId : searchRequest_.departure_.edges_) {
    auto label = CreateDepartureLabel(edgeId);
    searchObserver_.OnLabeled(label);
    queue_.push(label);
  }
}

inline Label Algorithm::PopMinLabel() {
  TryIncreasePopCount();

  metrics_.IncreasePopCounter();
  searchObserver_.OnLabelPop();

  Label minLabel = queue_.top();
  queue_.pop();

  return minLabel;
}

inline TRoute Algorithm::FinalizeRoute(const EdgeId &destinationEdgeId,
                                       const Label &minLabel,
                                       const TLabelIndex minLabelIndex) {
  auto finalLabel = CreateLabel(minLabel, minLabelIndex, destinationEdgeId);

  searchObserver_.OnLabeled(finalLabel);
  searchObserver_.OnSettled(finalLabel);

  const auto finalLabelIndex = StoreLabel(finalLabel);

  auto route = AssembleRoute(finalLabelIndex);
  searchObserver_.Finalize();

  return route;
}

inline void Algorithm::TryIncreasePopCount() {
  if (popCount_ >= popCountBudget_) {
    throw std::runtime_error("Budget for route search exceeded");
  }

  ++popCount_;
}

inline void Algorithm::VisitEdge(const Edge &incomingEdge,
                                 const Label &minLabel,
                                 const TLabelIndex minLabelIndex) {
  for (const auto &outgoingEdgeId :
       graph_.OutArcs(incomingEdge, minLabel.arrivalTime_)) {
    const auto &edge = graph_.GetEdge(outgoingEdgeId);

    metrics_.IncreaseVisitedEdges();

    if (IsSettled(edge)) {
      continue;
    }

    auto label = CreateLabel(minLabel, minLabelIndex, outgoingEdgeId);
    searchObserver_.OnLabeled(label);
    queue_.push(label);
  }
}

inline router::TMaybeRoute Algorithm::Search() {
  assert(settled_.empty());

  // Main search
  while (!queue_.empty()) {
    const auto minLabel = PopMinLabel();

    const auto &incomingEdge = graph_.GetEdge(minLabel.edgeId_);
    assert(incomingEdge.IsValid());

    if (!Settle(incomingEdge)) {
      searchObserver_.OnDiscarded(minLabel);
      continue;
    }

    searchObserver_.OnSettled(minLabel);
    const auto minLabelIndex = StoreLabel(minLabel);

    if (const auto destinationEdgeId =
            KnownEdgeToDestination(incomingEdge.To())) {
      return FinalizeRoute(*destinationEdgeId, minLabel, minLabelIndex);
    }

    VisitEdge(incomingEdge, minLabel, minLabelIndex);
  }

  searchObserver_.Finalize();
  return {};
}

inline bool Algorithm::Settle(const Edge &edge) {
  bool freshlySettled = false;

  // Try insertion if not there already
  std::tie(std::ignore, freshlySettled) =
      settled_.insert(CostModel::SettleEdgeString(edge));

  return freshlySettled;
}

inline bool Algorithm::IsSettled(const Edge &edge) {
  const auto it = settled_.find(CostModel::SettleEdgeString(edge));
  return (it != settled_.end());
}

template <typename Label>
inline TLabelIndex Algorithm::StoreLabel(Label &&label) {
  assert(label.IsValid());
  return labels_.Emplace(std::forward<Label>(label));
}

inline Label Algorithm::CreateDepartureLabel(const EdgeId &outEdgeId) const {
  const auto &outgoingEdge = graph_.GetEdge(outEdgeId);

  Label l(outEdgeId, outgoingEdge.ArrivalTime(searchRequest_.departure_.time_));
  l.cost_ = outgoingEdge.TravelTime();
  l.aStarEstimate_ = searchRequest_.costModel_.EstimateTravelTimeFrom(
      outgoingEdge.HeadCoordinate());

  return l;
}

inline Label Algorithm::CreateLabel(const Label &prevLabel,
                                    const TLabelIndex &prevLabelIndex,
                                    const EdgeId &outEdgeId) const {
  const auto &incomingEdge = graph_.GetEdge(prevLabel.edgeId_);
  const auto &outgoingEdge = graph_.GetEdge(outEdgeId);

  Label l(outEdgeId, outgoingEdge.ArrivalTime(prevLabel.arrivalTime_));
  l.predecessor_ = prevLabelIndex;
  l.aStarEstimate_ = searchRequest_.costModel_.EstimateTravelTimeFrom(
      outgoingEdge.HeadCoordinate());
  l.cost_ =
      prevLabel.cost_ + searchRequest_.costModel_.Cost(
                            incomingEdge, outgoingEdge, prevLabel.arrivalTime_);

  return l;
}

inline TRoute Algorithm::AssembleRoute(const TLabelIndex &finalLabelIndex) {
  const auto &route = BackTraceRoute(finalLabelIndex);

  return ConflateTripsAlongRoute(route);
}

inline TRoute Algorithm::BackTraceRoute(const TLabelIndex &finalLabelIndex) {
  TRoute route;
  for (auto label = labels_[finalLabelIndex];;
       label = labels_[label.predecessor_]) {
    const auto &edge = graph_.GetEdge(label.edgeId_);
    route.push_back(edge.CloneForArrivalTime(label.arrivalTime_));

    searchObserver_.OnFinalArc(label);

    if (label.IsDeparture()) {
      break;
    }
  };

  std::reverse(route.begin(), route.end());

  assert(route.front().From().IsDeparture());
  assert(route.back().To().IsDestination());

  return route;
}

inline Algorithm::TMaybeEdgeId Algorithm::KnownEdgeToDestination(
    const NodeId &from) {
  auto &destinationEdges = searchRequest_.destination_.edges_;
  assert(std::is_sorted(destinationEdges.begin(), destinationEdges.end(),
                        EdgeDepartureComp(graph_)));
  const auto comp = EdgeDepartureComp(graph_);
  const auto &it = std::lower_bound(destinationEdges.begin(),
                                    destinationEdges.end(), from, comp);

  if (it == destinationEdges.end() || comp(from, *it)) {
    return {};
  }

  return {*it};
}
}  // namespace detail
}  // namespace router
