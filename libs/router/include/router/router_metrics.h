/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "graph/types.h"

#include <cstddef>

namespace router {
struct RouterMetrics {
  std::size_t popCounter_{0};
  std::size_t visitedEdges_{0};
  std::size_t numEdges_{0};
  graph::TSeconds travelTime_{};
  long numRouteChanges_{0};

  void IncreasePopCounter() { ++popCounter_; }
  void IncreaseVisitedEdges() { ++visitedEdges_; }
};

inline RouterMetrics& operator+=(RouterMetrics& lhs, const RouterMetrics& rhs) {
  lhs.popCounter_ += rhs.popCounter_;
  lhs.visitedEdges_ += rhs.visitedEdges_;
  lhs.numEdges_ += rhs.numEdges_;
  lhs.travelTime_ += rhs.travelTime_;
  lhs.numRouteChanges_ += rhs.numRouteChanges_;
  return lhs;
}

inline RouterMetrics operator+(const RouterMetrics& lhs,
                               const RouterMetrics& rhs) {
  RouterMetrics result = lhs;
  result += rhs;
  return result;
}

}  // namespace router
