/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include "graph/edge.h"   // for Edge
#include "graph/graph.h"  // for graph
#include "graph/node.h"   // for NodeId
#include "graph/types.h"  // for TTimePoint

#include <functional>  // for reference_wrapper
#include <iosfwd>      // for std::ostream
#include <memory>
#include <optional>
#include <string>

namespace router {

struct RouterMetrics;

using TRoute = std::vector<graph::Edge>;
using TMaybeRoute = std::optional<TRoute>;
using TMaybeMetricsRef = std::optional<std::reference_wrapper<RouterMetrics>>;
using TMaybeOutStreamRef = std::optional<std::reference_wrapper<std::ostream>>;

namespace detail {
struct SearchRequest;
}

class RouterData {
 public:
  explicit RouterData(const graph::Graph& graph) : graph_{graph} {}

  [[nodiscard]] const graph::Graph& GetGraph() const { return graph_; }

  const graph::Graph& graph_;
  graph::TTimePoint departureTime_;
  graph::TPlace departure_;
  graph::TPlace destination_;
  TMaybeMetricsRef metrics_;
  TMaybeOutStreamRef searchObserver_;
  /**
   * Set to a default of 4 by experimentation.
   * This overestimates the cost by design
   * by using an effective average speed of 20 kph
   */
  int aStarFactor_ = 4;
};

// The dtor is in fact defaulted in the cpp file
// It needs to be outside of the header for the forward declared pImpl
// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
class Router : private RouterData {
 private:
  using Graph = graph::Graph;

  class Args : public RouterData {
   public:
    explicit Args(const Graph& graph) : RouterData(graph) {}

    template <typename T>
    Args& Departure(T&& departure) {
      departure_ = std::forward<T>(departure);
      return *this;
    }

    template <typename T>
    Args& Destination(T&& departure) {
      destination_ = std::forward<T>(departure);
      return *this;
    }

    Args& DepartureTime(const graph::TTimePoint& departureTime) {
      departureTime_ = departureTime;
      return *this;
    }

    Args& AStarFactor(const int aStarFactor) {
      aStarFactor_ = aStarFactor;
      return *this;
    }

    Args& RouterMetrics(RouterMetrics& metrics) {
      metrics_ = metrics;
      return *this;
    }

    Args& SearchSpaceStream(std::ostream& stream) {
      searchObserver_ = stream;
      return *this;
    }
  };

 public:
  static Args WithArgs(const Graph& graph) { return Args(graph); }

  explicit Router(Args args);

  ~Router();

  TMaybeRoute FindRoute();

  TMaybeRoute FindRoute(int maxPopCount);

  void PrintRoute() const;
  void PrintRoute(std::ostream& stream) const;

  [[nodiscard]] std::string RouteToString() const;

 private:
  struct Impl;
  std::unique_ptr<Impl> impl_;

  TMaybeRoute route_;
};

}  // namespace router
