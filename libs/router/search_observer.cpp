/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "router/detail/search_observer.h"

#include "graph/distance.h"                // for Distance
#include "graph/edge.h"                    // for Edge
#include "graph/types.h"                   // for Point
#include "graph/types_io.h"                // for Formatted
#include "router/detail/geojson_writer.h"  // for GeoJsonWriter

#include <units.h>

#include <cassert>  // for assert
#include <chrono>
#include <ostream>  // for string, ostream
#include <utility>  // for pair

using namespace units::literals;

namespace router::detail {

void SearchObserver::OnLabelPop() { ++step_; }

void SearchObserver::OnSettled(const Label& label) {
  Timeframe settledTimeframe;
  settledTimeframe.start_ = step_;

  const auto inserted =
      settled_.insert(std::make_pair(label, settledTimeframe));

  (void)inserted;
  assert(inserted.second && "Should have been freshly settled");

  MarkEndOfLabel(label);
}

void SearchObserver::MarkEndOfLabel(const Label& label) {
  auto labeledTimeFrameIt = labeled_.find(label);
  if (labeledTimeFrameIt == labeled_.end()) {
    assert(false && "Failed to find label in timeframe");
    return;
  }
  (*labeledTimeFrameIt).second.end_ = step_;
}

void SearchObserver::ExtendEndToMaxStart(TLabelCollection& collection) {
  const auto maxStart = std::accumulate(
      collection.begin(), collection.end(), -1, [](int& acc, const auto& val) {
        return std::max(acc, val.second.start_);
      });

  std::for_each(collection.begin(), collection.end(),
                [maxStart](auto& val) { val.second.end_ = maxStart; });
}

SearchObserver::LineExtent SearchObserver::GetLineExtent(
    const SearchObserver::TEdgeId& edgeId) const {
  const auto& edge = graph_.GetEdge(edgeId);
  const auto& tailCoordinate =
      edge.From().IsDeparture()
          ? departure_
          : graph_.UnderlyingGraph().StopCoordinates(edge.From());
  const auto& headCoordinate = edge.HeadCoordinate();
  return std::make_tuple(tailCoordinate, headCoordinate);
}

void SearchObserver::OnDiscarded(const Label& label) { MarkEndOfLabel(label); }

void SearchObserver::OnLabeled(const Label& label) {
  Timeframe tf;
  tf.start_ = step_;
  const auto inserted = labeled_.insert(std::make_pair(label, tf));
  (void)inserted;
  assert(inserted.second && "This should be the first insert");
}

void SearchObserver::OnFinalArc(const Label& label) {
  ++step_;
  Timeframe tf;
  tf.start_ = step_;
  const auto inserted = finalRoute_.insert(std::make_pair(label, tf));
  // TODO fishy
  (void)inserted;
  assert(inserted.second &&
         "Inserting a final arc should not fail given a route with no loops");
}

void SearchObserver::Finalize() {
  assert(!isFinalized_);
  ExtendEndToMaxStart(settled_);
  ExtendEndToMaxStart(finalRoute_);
  isFinalized_ = true;
}

namespace {
Point ConvertCoordinate(const graph::Point& point) {
  Point p{};
  p.lon_ = point.lon_;
  p.lat_ = point.lat_;
  return p;
}

const constexpr auto kMinLineDistance = 10_m;

Geometry MakeGeometry(const graph::Point& tailCoordinate,
                      const graph::Point& headCoordinate) {
  const auto lineDistance = graph::Distance(tailCoordinate, headCoordinate);

  if (lineDistance <= kMinLineDistance) {
    return ConvertCoordinate(headCoordinate);
  }

  Line l{};
  l.from_ = ConvertCoordinate(tailCoordinate);
  l.to_ = ConvertCoordinate(headCoordinate);
  return l;
}

}  // namespace

FeatureCollection SearchObserver::MakeCollection(
    const char* name, const SearchObserver::TLabelCollection& labelCollection,
    const char* color) const {
  FeatureCollection fc;
  fc.name_ = name;
  fc.color_ = color;

  // TODO too long function move out?
  const auto transformFun = [this](const std::pair<Label, Timeframe>& pair) {
    TimeboxedFeature feature;
    const auto& label = pair.first;
    feature.timeframe_ = pair.second;
    const auto& extent = GetLineExtent(label.edgeId_);
    feature.geometry_ = MakeGeometry(std::get<0>(extent), std::get<1>(extent));

    const auto& edge = graph_.GetEdge(label.edgeId_);
    feature.cost_ =
        std::chrono::duration_cast<std::chrono::seconds>(label.cost_).count();
    feature.aStarEstimate_ =
        std::chrono::duration_cast<std::chrono::seconds>(label.aStarEstimate_)
            .count();
    feature.arrival_ = graph::Formatted(label.arrivalTime_);
    feature.departure_ = graph::Formatted(edge.DepartureTime());
    feature.travelTime_ =
        std::chrono::duration_cast<std::chrono::seconds>(edge.TravelTime())
            .count();
    // TODO internal knowledge of how to describe a walking, disembark, etc edge
    // should be encapsulated
    feature.tripId_ = edge.TripId();
    if (!feature.tripId_.empty()) {
      feature.tripShortName_ = graph_.UnderlyingGraph().Describe(edge.TripId());
    }
    return feature;
  };

  std::transform(labelCollection.begin(), labelCollection.end(),
                 std::back_inserter(fc.features_), transformFun);

  return fc;
}

void SearchObserver::Write(std::ostream& stream) const {
  assert(isFinalized_);

  GeoJsonWriter jsonWriter;

  jsonWriter.AppendCollection(MakeCollection("labeled", labeled_, "red"));
  jsonWriter.AppendCollection(MakeCollection("settled", settled_, "yellow"));
  jsonWriter.AppendCollection(MakeCollection("route", finalRoute_, "green"));

  stream << jsonWriter.Serialize();
}

}  // namespace router::detail
