/**
 * Copyright (c) 2020 Kai Hoewelmeyer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "env_helper/mappath.h"  // for GetMapPathFromEnv
#include "graph/gtfs_graph.h"    // for GraphDatabase
#include "graph/node.h"          // for NodeId
#include "graph/types.h"         // for TTimePoint
#include "router/router.h"       // for TMaybeRoute, Router, Router::Args

#include <cassert>      // for assert
#include <chrono>       // for milliseconds, seconds, duration_cast
#include <ctime>        // for tm
#include <ctime>        // for localtime, mktime
#include <cxxabi.h>     // for __forced_unwind
#include <iomanip>      // for get_time, operator<<, operator>>
#include <iostream>     // for operator<<, char_traits, basic_ost...
#include <random>       // for mt19937, random_device, uniform_in...
#include <sstream>      // for stringstream
#include <string>       // for operator<<, string
#include <type_traits>  // for enable_if<>::type

namespace {
std::chrono::system_clock::time_point IsoToTimepoint(const std::string& value) {
  std::tm tm = {};
  std::stringstream ss(value);
  // TODO duplicated format string
  ss >> std::get_time(&tm, "%Y%m%dT%H%M%S");
  assert(!ss.fail());
  return std::chrono::system_clock::from_time_t(std::mktime(&tm));
}
}  // namespace

using namespace router;
using namespace graph;

using TGraph = graph::GTFSGraph;

namespace fuzzer {
class Fuzzer {
 public:
  using TMilliseconds = std::chrono::microseconds;

  explicit Fuzzer(const Graph& graph) : graph_{graph} {}

  void Next();
  bool Calculate();

  void PrintArgs() const;
  void PrintResult() const;

 private:
  [[nodiscard]] NodeId RandomStation() const;
  [[nodiscard]] static TTimePoint RandomTime();

  const Graph& graph_;
  NodeId departure_;
  NodeId destination_;
  TTimePoint departureTime_;
  TMilliseconds duration_{0};
  TMaybeRoute maybeRoute_;

  static const TTimePoint kMinimumTime;
  static const TTimePoint kMaximumTime;
};

const TTimePoint Fuzzer::kMinimumTime = IsoToTimepoint("20180101T000000Z");
const TTimePoint Fuzzer::kMaximumTime = IsoToTimepoint("20181231T235959Z");

void Fuzzer::Next() {
  departure_ = RandomStation();
  destination_ = RandomStation();
  departureTime_ = RandomTime();
}

bool Fuzzer::Calculate() {
  using namespace std::chrono_literals;
  constexpr auto kMaxTimeToRouteFound = 32ms;
  constexpr auto kMaxTimeToRouteNotFound = 5s;

  const auto startTime = std::chrono::system_clock::now();

  router::Router router(router::Router::WithArgs(graph_)
                            .Departure(departure_)
                            .Destination(destination_)
                            .DepartureTime(departureTime_));

  maybeRoute_ = router.FindRoute();

  const auto endTime = std::chrono::system_clock::now();
  duration_ = std::chrono::duration_cast<std::chrono::milliseconds>(endTime -
                                                                    startTime);

  return maybeRoute_ ? duration_ <= kMaxTimeToRouteFound
                     : duration_ <= kMaxTimeToRouteNotFound;
}

TTimePoint Fuzzer::RandomTime() {
  const auto min = std::chrono::duration_cast<std::chrono::seconds>(
                       kMinimumTime.time_since_epoch())
                       .count();
  const auto max = std::chrono::duration_cast<std::chrono::seconds>(
                       kMaximumTime.time_since_epoch())
                       .count();
  std::random_device rd;
  std::mt19937 rng(rd());
  std::uniform_int_distribution<decltype(min)> distr(min, max);

  return std::chrono::system_clock::from_time_t(
      distr(rng));  // TODO not portable? time_t type not guaranteed
}

NodeId Fuzzer::RandomStation() const { return graph_.RandomNode(); }

void Fuzzer::PrintArgs() const {
  const auto time = std::chrono::system_clock::to_time_t(departureTime_);
  std::cout << "Departure: " << graph_.Describe(departure_)
            << ", Destination: " << graph_.Describe(destination_)
            << ", DepartureTime: "
            // TODO also use this for the other operator<<'s impls
            << std::put_time(std::localtime(&time), "%FT%T%z") << std::endl;
}

void Fuzzer::PrintResult() const {
  std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(duration_)
                   .count()
            << " ms ";
  if (!maybeRoute_) {
    std::cout << "and no route found";
  }
  std::cout << std::endl;
}
}  // namespace fuzzer

int main() {
  const TGraph g{env_helper::GetMapPathFromEnv()};

  fuzzer::Fuzzer f{g};

  std::cout << "Looking for calculation that is taking too long" << std::endl;

  int counter = 0;
  do {
    f.Next();
    std::cout << ++counter << ": ";
    f.PrintArgs();
  } while (f.Calculate());

  std::cout << "Calculated " << counter
            << " routes before finding a too long calculation" << std::endl;

  f.PrintResult();

  return 0;
}
