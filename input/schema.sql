CREATE TABLE agency(agency_id TEXT,agency_name TEXT NOT NULL,
                    agency_url TEXT NOT NULL,
                    agency_timezone TEXT NOT NULL, agency_lang TEXT, agency_phone TEXT,
                    agency_fare_url TEXT, agency_email TEXT);
CREATE TABLE calendar_dates(service_id TEXT NOT NULL,
                    date NUMERIC NOT NULL,exception_type NUMERIC NOT NULL);
CREATE TABLE calendar(service_id text NOT NULL, monday boolean NOT NULL,
                    tuesday boolean NOT NULL,
                    wednesday boolean NOT NULL, thursday boolean NOT NULL,
                    friday boolean NOT NULL, saturday boolean NOT NULL,
                    sunday boolean NOT NULL, start_date numeric NOT NULL,
                    end_date numeric NOT NULL);
CREATE TABLE routes(route_id TEXT NOT NULL,agency_id TEXT,
                    route_short_name TEXT NOT NULL,
                    route_long_name TEXT NOT NULL,route_desc TEXT,route_type NUMERIC,
                    route_url TEXT,route_color TEXT,route_text_color TEXT);
CREATE TABLE shapes(shape_id TEXT NOT NULL,shape_pt_lat REAL NOT NULL,
                    shape_pt_lon REAL NOT NULL,
                    shape_pt_sequence NUMERIC NOT NULL, 
                    shape_dist_travelled NUMERIC);
CREATE TABLE stops(stop_id text UNIQUE,stop_code TEXT,stop_name TEXT NOT NULL,
                   stop_desc TEXT,stop_lat REAL NOT NULL,stop_lon REAL NOT NULL,
                   zone_id NUMERIC,stop_url TEXT,location_type
                   INT,parent_station text, stop_timezone TEXT,
                   wheelchair_boarding INT, level_id TEXT, platform_code TEXT);
CREATE TABLE transfers( from_stop_id TEXT NOT NULL, to_stop_id TEXT NOT NULL,
transfer_type int NOT NULL, min_transfer_time int, from_route_id TEXT, to_route_id TEXT, from_trip_id TEXT, to_trip_id TEXT);
CREATE TABLE trips(route_id TEXT NOT NULL,service_id TEXT NOT NULL,
                  trip_id TEXT NOT NULL,
                  trip_headsign TEXT,trip_short_name TEXT, direction_id NUMERIC,
                  block_id TEXT,shape_id TEXT, wheelchair_accessible NUMERIC,
                  bikes_allowed NUMERIC);
CREATE TABLE stop_times_joined(
  trip_id TEXT,
  service_id TEXT,
  route_id TEXT,
  departure_time INT,
  arrival_time INT,
  departure_stop_id TEXT,
  arrival_stop_id TEXT,
  head_lat REAL,
  head_lon REAL,
  effective_weekday,
  has_removals
);
CREATE INDEX stop_stop_id on stops(stop_id);
CREATE INDEX stop_stop_lat on stops(stop_lat);
CREATE INDEX stop_stop_lon on stops(stop_lon);
CREATE INDEX transfersIdx on transfers(from_stop_id, min_transfer_time, to_stop_id);
CREATE INDEX routes_route_id on routes(route_id);
CREATE INDEX trips_trip_id on trips(trip_id);
CREATE INDEX calendar_service_id on calendar(service_id);
CREATE INDEX calendar_dates_service_id_date on calendar_dates(service_id, date);
CREATE INDEX stop_times_joined_idx ON
  stop_times_joined(departure_stop_id, departure_time);
