#!/usr/bin/env python3

# Copyright (c) 2020 Kai Hoewelmeyer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from flask import Flask, render_template
import json
import faulthandler

faulthandler.enable()

app = Flask(__name__)

import transport_router as tr

@app.route('/stations/<substr>')
def stations(substr):
    return json.dumps(tr.stops_with_name(substr))

@app.route('/')
def route_search():
    return app.send_static_file('routesearch.html')

def searchfront(from_kwargs, to_kwargs, conversion_func, time_t):
    source = conversion_func(**from_kwargs)
    target = conversion_func(**to_kwargs)
    return tr.searchfront(source, target, time_t)

@app.route('/searchfront/<float:from_lat>,<float:from_lon>:<float:to_lat>,<float:to_lon>@<int:time_t>')
def searchfront_coordinate(from_lat, from_lon, to_lat, to_lon, time_t):
    return searchfront({"lat": from_lat, "lon": from_lon},
                       {"lat": to_lat, "lon": to_lon},
                       tr.Coordinate,
                       time_t)

@app.route('/searchfront/<from_station_id>,<to_station_id>@<int:time_t>')
def searchfront_station(from_station_id, to_station_id, time_t):
    return searchfront({"station_id": from_station_id},
                       {"station_id": to_station_id},
                       tr.Station,
                       time_t)

def route(from_kwargs, to_kwargs, conversion_func, time_t):
    source = conversion_func(**from_kwargs)
    target = conversion_func(**to_kwargs)
    route_description = tr.route(source, target, time_t)
    result = {"routeDescription": route_description}
    return json.dumps(result)

@app.route('/route/<from_station_id>,<to_station_id>@<int:time_t>')
def route_station(from_station_id, to_station_id, time_t):
    return route({"station_id": from_station_id},
                 {"station_id": to_station_id},
                 tr.Station,
                 time_t)

@app.route('/route/<float:from_lat>,<float:from_lon>:<float:to_lat>,<float:to_lon>@<int:time_t>')
def route_coordinate(from_lat, from_lon, to_lat, to_lon, time_t):
    return route({"lat": from_lat, "lon": from_lon},
                 {"lat": to_lat, "lon": to_lon},
                 tr.Coordinate,
                 time_t)

@app.route('/map')
def map():
    return render_template('searchfront.html')

@app.route('/map/<from_node>,<to_node>@<int:time_t>')
def map_for_request(from_node, to_node, time_t):
    preloadUrl = "/searchfront/%s,%s@%d" % (from_node, to_node, time_t)
    return render_template('searchfront.html', preloadUrl = preloadUrl)
