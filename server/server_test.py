#!/usr/bin/env python3

# Copyright (c) 2020 Kai Hoewelmeyer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import unittest
import datetime
import time

from server import app


class TransportRouterServerTest(unittest.TestCase):

    def test_home(self):
        tester = app.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)

    def test_stations(self):
        tester = app.test_client(self)
        response = tester.get('/stations/Adler', content_type='html/text')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(b'Adlershof' in response.data)

    def test_basic_route(self):
        tester = app.test_client(self)
        adlershof = (52.4358, 13.5406)
        treptower_park = (52.4940, 13.4620)

        def next_monday():
            today = datetime.datetime.now()
            days_to_monday = datetime.timedelta(days=(7-today.weekday()))
            monday = today + days_to_monday
            monday = monday.replace(hour=8, minute=0, second=0)
            return time.mktime(monday.timetuple())

        departure = next_monday()
        url = "/route/{},{}:{},{}@{}".format(adlershof[0], adlershof[1],
                                             treptower_park[0], treptower_park[1], int(departure))
        response = tester.get(url, content_type='html/text')
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
