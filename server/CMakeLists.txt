if(PTR_SANITIZER)
  # Running under sanitizers not supported:
  # Since python is build without the sanitizer, shared sanitizer libraries
  # have to be used. This requires the use of LD_PRELOAD for the sanitizer
  # runtime. That is incompatible with executables that already dynamically
  # load the sanitizer run time.
  # In other words, there is no reusable configuration of compiler / linker
  # flags that works for both the python and the normal executable use case.
  # Even when enabling the sanitizer correctly, cpython itself has leaks that
  # are reported and would require a suppression list.
  # The maintenance cost of this configuration is not worth the benefits.
  set(default_value OFF)
else()
  set(default_value ON)
endif()

option(ROUTING_SERVER "Python frontend" ${default_value})

if(NOT ROUTING_SERVER)
    return()
endif()

if(TARGET map)
  get_target_property(ROUTING_MAP_PATH map MAP_PATH)
endif()

get_target_property(PYTHON_MODULE_PATH transport_router LIBRARY_OUTPUT_DIRECTORY)

set(SCRIPT_COMMAND "flask run")
configure_file(run_python_app.sh.in start_server.sh @ONLY)

include(ExternalMap)
if(ROUTING_MAP)
    set(PYTEST_JUNIT_ARGS)
    if(ROUTING_TEST_JUNIT)
        set(PYTEST_JUNIT_ARGS "--junitxml ${CMAKE_CURRENT_BINARY_DIR}/TEST-pytest.xml")
    endif()
    set(SCRIPT_COMMAND "pytest ${PYTEST_JUNIT_ARGS} server_test.py")
    configure_file(run_python_app.sh.in run_tests.sh @ONLY)
    add_test(python_server ${CMAKE_CURRENT_BINARY_DIR}/run_tests.sh)
endif()

install(DIRECTORY ${CMAKE_CURRENT_LIST_DIR}/
        DESTINATION ${PYTHON_LIB_PATH}/ptr_server
        FILES_MATCHING
        PATTERN "__pycache__/*" EXCLUDE
        PATTERN "static/*"
        PATTERN "templates/*"
        PATTERN "*.py"
        )

set(REQUIREMENTS_FILE "${CMAKE_CURRENT_LIST_DIR}/requirements.txt")
configure_file(install_requirements.cmake.in "${CMAKE_CURRENT_BINARY_DIR}/install_requirements.cmake")
install(SCRIPT "${CMAKE_CURRENT_BINARY_DIR}/install_requirements.cmake")
