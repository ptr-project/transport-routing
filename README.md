![logo](images/logo.png) — public transport router
==============================

[![pipeline status](https://gitlab.com/ptr-project/transport-routing/badges/master/pipeline.svg)](https://gitlab.com/ptr-project/transport-routing/-/commits/master)
[![Contributor
Covenant](https://img.shields.io/badge/Contributor%20Covenant-v1.4%20adopted-ff69b4.svg)](code-of-conduct.md)

A C++ research routing engine using GTFS-based data.

This repository currently contains the map toolchain to create new maps, the
front-end, and the actual C++ code for the routing engine.

Currently, only GTFS for Berlin data is tested and considered.

Building & Running
-------------------

ptr uses a standard CMake-based building process. A compiler, CMake, Conan, and
Python are required. E.g., for Ubuntu the requirements can be installed as
follows:
```
apt-get update && apt-get install -y build-essential cmake ninja-build python3 python3-pip
python3 -m pip install conan
```

```
git clone https://gitlab.com/kaihowl/transport-routing.git
cd transport-routing
mkdir build
cd build
cmake -G Ninja -DROUTING_MAP=ON ..
ninja
```

This will fetch the repo, download and install dependencies with
[Conan](https://conan.io/) into `~/.conan`, and finally build
the project. The `ROUTING_MAP` option enables downloading the default map with
a Berlin-based dataset.

### Using the Lockfile and Updating Dependencies

The conanfile.py uses version ranges to specify dependencies. The concrete
versions are pinned in the conan.lock file. When using a pure CMake invocation
(as detailed above), the pinned dependencies of base lockfile are taken into
account.

When using conan, a full lockfile has to be derived from the base lockfile.

```
conan lock create -l conan.lock --lockfile-out conan.lock.full conanfile.py
conan install conanfile.py -l conan.lock.full ...
```

The base lockfile can be updated as follows.

```
conan lock create -u conanfile.py --base --lockfile-out conan.lock --build '*'
```

Running
-------

Tests can be run with `ctest` or directly by invoking your make program:

```
ninja test
```

Running the server with the front end and search space explorer is done by
running a helper shell script:

```
cd transport-routing/build/
./server/start_server.sh
```

This requires that the cmake option `ROUTING_MAP` is enabled.

License
-------

ptr is licensed under the [MIT license](LICENSE).
Third-party software used is detailed in [NOTICE](NOTICE).
