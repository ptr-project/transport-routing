# Copyright (c) 2020 Kai Hoewelmeyer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from conans import tools, ConanFile, CMake
from conans.errors import ConanInvalidConfiguration

import os
import json


class TransportRoutingConan(ConanFile):
    name = "transport_routing"
    license = "<Put the package license here>"
    author = "<Put your name here> <And your email here>"
    url = "<Package recipe repository url here, for issues about the package>"
    description = "<Description of Sqliteutils here>"
    topics = ("<Put some tag here>", "<here>", "<and here>")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False],
               "fPIC": [True, False],
               "build_testing": [True, False],
               "with_map": [True, False],
               }
    default_options = {"shared": False,
                       "fPIC": True,
                       "build_testing": False,
                       "with_map": False,
                       "boost:header_only": True,
                       "sqlite3:threadsafe": 0,
                       "date:use_system_tz_db": True}
    generators = ["cmake", "cmake_find_package", "cmake_paths"]
    requires = [
        "pybind11/[2]",
        "boost/[1]",
        "units/[2]",
        "catch2_extended/2.13.6@ptr-project+conan-packages/stable",
        "nlohmann_json/[3]",
        "date/[3]",
        "sqlite3/[3]",
        "sqlite_utils/0.3.0@ptr-project+conan-packages/stable",
    ]
    build_requires = ["cmake_utils/[*]@ptr-project+conan-packages/stable"]
    scm = {
        "type": "git",
        "url": "https://gitlab.com/ptr-project/transport-routing.git",
        "revision": "auto",
    }

    def config_options(self):
        tools.check_min_cppstd(self, "17")

    def _configure_cmake(self):
        cmake = CMake(self)
        cmake.configure(source_folder="./",
                defs={
                    "BUILD_TESTING": self.options.build_testing,
                    "ROUTING_MAP": self.options.with_map,
                })
        return cmake

    def build(self):
        cmake = self._configure_cmake()
        cmake.build()
        if self.options.build_testing:
            cmake.test()

    def package(self):
        cmake = self._configure_cmake()
        cmake.install()

    def deploy(self):
        pass

    def package_info(self):
        pass
