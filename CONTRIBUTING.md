# Introduction

## Hey there!

First off, thank you for considering to contribute to ptr. We are still in the early stages and only basic features work.
If you are willing to spend time on improving the code base with more features, bugfixes, or other small improvements, we welcome you
to help us and are excited to have you.

Following these guidelines helps to communicate that you respect the time of the developers managing and developing this open source project. In return, they should reciprocate that respect in addressing your issue, assessing changes, and helping you finalize your pull requests.

## Community

We are available via email
* on the official devel mailinglist ptr-devel@googlegroups.com
* and a private mailing list for code of conduct violations and security issues ptr-maint@googlegroups.com

## What are we looking for?

All pull requests improving the current state of the source code, adding features, creating documentation & examples, or fixing bugs are welcome changes so as long as they follow the ground rules.

## What are we not looking for?

Do *not* use the issue tracker for questions. Instead [reach](#community) out to us.

Pull requests for fixes in toolchains not tested under CI are *not* considered and will be closed. Adding support for a toolchain is a feature and should be handled accordingly.

## Ground Rules

*Responsibilities*
* Adhere to the [code of conduct](CODE_OF_CONDUCT.md)
* Mark pull requests clearly as WIP if they do not need active review
* Create issues for any major changes and enhancements that you wish to make. Discuss things transparently and get community feedback.
* Keep pull requests as small as possible
  * Unrelated changes should not be part of the same pull-request
  * Bigger refactorings can be done piecemeal
* Add new dependencies sparingly
* Add focussed, small unit tests

*In return*, we will
* review pull requests in a timely fashion
* clearly out line problems and offer ideas on how to solve them

## Your First Contribution

Working on your first Pull Request? You can learn how from this *free* series, [How to Contribute to an Open Source Project on GitHub](https://egghead.io/series/how-to-contribute-to-an-open-source-project-on-github). Since this project is hosted on Gitlab, a [comparison of different terms](https://about.gitlab.com/2017/09/11/comparing-confusing-terms-in-github-bitbucket-and-gitlab/) might help to still be able to use above guide for Gitlab as well.

## How to report a bug
If you find a security vulnerability, do NOT open an issue. Send a message to the private mailing list (see [community](#community)).

When filing an issue, make sure to answer these questions:

* Have you checked if the bug already is filed?
* What version of the software are you using (tag or commit hash)
* Are you running a supported toolchain? If so, please specify.
* Does the issue occur on a specific data set? If so, please specify.
* What did you do?
* What did you expect to see?
* What did you see instead?

For general questions should please contact the [community](#community) first. If it is a bug after all, we will point this out and help with filing the bug.

## How to suggest a feature or enhancement

Check the [roadmap](ROADMAP.md) first if the feature is already on the horizon. If not, go have a chat with the [community](#community). We will help you take the first steps on how to get a new feature on track.

## Code review process

The code has to be reviewed by a project maintainer. Merging of the pull request is at the sole discression of the maintainers. To avoid waste, we invite you to engange in a discussion with [the community](#community) before opening a pull request.

