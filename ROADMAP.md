Roadmap
=======

Run-time customizable code model (spike)
----------------------------------------

This spike should establish the implications (run-time overhead) of adding
a customizable cost model for route search. The current focus consists of
adding the cost function as a python function from the web interface. This
might involve checking JIT compilation for the Python code. If this proves
succesful, the function to produce the `SettleEdgeString` is another candidate
to allow customization.

Improve transfer handling
--------------------------

There are two current problems in the implementation:
* Transfers from `tranfers.txt` are considered without checking the
  `transfer_type` (cf
  https://developers.google.com/transit/gtfs/reference/#transferstxt)
* Transfers are only gathered from `transfers.txt`
  * A valid GTFS implementation must consider transfer based on geo distance of
    stations as well
