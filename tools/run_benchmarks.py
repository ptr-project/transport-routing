#!/usr/bin/env python3

# Copyright (c) 2020 Kai Hoewelmeyer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import print_function

import tempfile
import subprocess
import sys
import shutil
import os.path
import os
import argparse
import cmake
import git
from io import StringIO
import pandas as pd


# Mapping of targets to binaries
# TODO could be obtained by the cmake fileapi for example instead
BENCHMARKS = {
        "cache_benchmark": "libs/cache/benchmark/cache_benchmark",
        "graph_benchmark": "libs/graph/benchmark/graph_benchmark",
        "router_benchmark": "libs/router/benchmark/router_benchmark"
        }

DEFAULT_GROUP=['name', 'run']

def build(branch, targets):
    """ Build supplied targets in new temporary directory and return name of that directory """
    source_dir = tempfile.mkdtemp()
    git.makeworkdir(source_dir, branch)
    build_dir = tempfile.mkdtemp()
    cmake.configure(source_dir, build_dir, {"CMAKE_BUILD_TYPE": "Release"})
    cmake.build(build_dir, targets=targets)
    shutil.rmtree(source_dir)
    return build_dir


def run_benchmark(binary):
    """ Run comparison and return resulting csv entries """
    command = [binary]
    command += ["-r benchmark-csv"]
    csv = subprocess.check_output(command)
    return csv.decode('utf8')


def make_pandas_frame(csv_string, run_identifier):
    df = pd.read_csv(StringIO(csv_string), header=0)
    df['run'] = run_identifier
    df = df.apply(lambda r: r.append([to_series(r['statistics'])]), axis=1)
    return df


def keep_samples_with_lowest_variance_in_group(df):
    # TODO this assumes that the outlier variance is unique between iterations
    # i.e., two iterations with different samples will never yield the same
    # outlier variance
    # While theoretically possible, we assume this event to have a low
    # probability of ever occurring.
    filtered = df.groupby(DEFAULT_GROUP).apply(lambda d: d[d.outlier_variance == d.outlier_variance.min()])
    return filtered.reset_index(drop=True)


def plot(dataframe, output_basename, outliers=False):
    plot = dataframe.boxplot(column=['sample(ns)'], by=DEFAULT_GROUP, bootstrap=10000, notch=True, showfliers=outliers, vert=False)
    filename = output_basename
    if outliers:
        base, ext = os.path.splitext(output_basename)
        filename = base + "-outliers" + ext
    plot.get_figure().savefig(filename, bbox_inches='tight')


def save_to_csv(dataframe, output_basename):
    """ Save all supplied dataframe to a csv file """
    base, ext = os.path.splitext(output_basename)
    filename = base + ".csv"
    dataframe.to_csv(filename)


def to_series(string):
    if type(string) != str:
        return pd.Series(dict())

    if len(string) <= 4:
        return pd.Series(dict())

    kv = dict([tuple(kv.split(' := ')) for kv in string.split(";")])
    return pd.Series(kv)


def run(committish, benchmark_name):
    """
    Run benchmark_name from BENCHMARKS on committish.
    Returns pandas frame with results
    """
    print(f"Compiling benchmark for {committish}")
    build_dir = build(committish, BENCHMARKS.keys())
    print(f"Running benchmark for {committish}")
    binary = os.path.join(build_dir, BENCHMARKS[benchmark_name])
    df = make_pandas_frame(run_benchmark(binary), committish)
    shutil.rmtree(build_dir)
    return keep_samples_with_lowest_variance_in_group(df)


def main(committish, output):
    dataframes = []
    for commit in committish:
        for benchmark_name in BENCHMARKS.keys():
            dataframes.append(run(commit, benchmark_name))
    all_df = pd.concat(dataframes)
    save_to_csv(all_df, output)
    plot(all_df, output)
    plot(all_df, output, outliers=True)


if __name__ == "__main__":
    if not "ROUTING_MAP" in os.environ:
        print("Missing ROUTING_MAP environment variable", file=sys.stderr)
        sys.exit(1)
    parser = argparse.ArgumentParser()
    parser.add_argument("committish", nargs="+", help="git committish to run benchmarks on and compare")
    parser.add_argument("--output", "-o", default="benchmark.pdf", help="File to save the plot to. Passed to matplotlib's savefig. Understands different file extensions.")
    args = parser.parse_args()
    main(args.committish, args.output)
