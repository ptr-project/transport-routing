#!/usr/bin/env python3

# Copyright (c) 2020 Kai Hoewelmeyer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import print_function

import tempfile
import subprocess
import sys
import shutil
import os.path
import os
import argparse
import cmake
import git
import time
import ninjatrace


def build(branch):
    source_dir = tempfile.mkdtemp()
    git.makeworkdir(source_dir, branch)
    build_dir = tempfile.mkdtemp()
    options = {
            "CMAKE_BUILD_TYPE": "Release",
            "ROUTING_CCACHE": "OFF"}
    cmake.configure(source_dir, build_dir, options)

    start = time.time()
    cmake.build(build_dir, target='all')
    duration_seconds = time.time() - start
    print(">>> Branch %s took %.2f to compile" % (branch, duration_seconds))

    shutil.rmtree(source_dir)
    return build_dir


def get_log(ref_spec):
    logname = ".ninja_log"
    builddir = build(ref_spec)
    src = os.path.join(builddir, logname)
    dest = os.path.join("./", ref_spec + ".log")
    shutil.copy(src, dest)
    shutil.rmtree(builddir)
    return ninjatrace.parse(dest)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("before")
    parser.add_argument("after")
    args = parser.parse_args()
    before_times = get_log(args.before)
    after_times = get_log(args.after)
    for key, value in before_times.items():
        if key in after_times:
            percentage = after_times[key] * 1.0 / value * 100.0
            print("%s: %.2f ms (%.3f%%)" % (key, after_times[key], percentage))
        else:
            print("Deleted %s: %.2f ms" % (key, before_times[key]))
