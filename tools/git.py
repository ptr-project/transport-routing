# Copyright (c) 2020 Kai Hoewelmeyer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import distutils.spawn
import subprocess

def binary():
    return distutils.spawn.find_executable("git")


def findrootdir():
    command = [binary()]
    command += ["rev-parse", "--show-toplevel"]
    return subprocess.check_output(command).rstrip()


def clone(directory):
    command = [binary()]
    command += ["clone"]
    command += ["--no-checkout"]
    command += [findrootdir()]
    command += [directory]
    subprocess.check_call(command)


def checkout(directory, committish):
    command = [binary()]
    command += ["checkout"]
    command += [committish]
    subprocess.check_call(command, cwd=directory)


def makeworkdir(directory, committish):
    clone(directory)
    checkout(directory, committish)
