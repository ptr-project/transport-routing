# Copyright (c) 2020 Kai Hoewelmeyer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import distutils.spawn
import subprocess


def binary():
    return distutils.spawn.find_executable("cmake")


def configure(source_dir, build_dir, options):
    command = [binary()]
    command += ["-G", "Ninja"]
    for key, value in options.items():
        command += ["-D", "%s=%s" % (key, value)]
    command += [source_dir]
    subprocess.check_call(command, cwd=build_dir)


def build(build_dir, target=None, targets=None):
    """ Build given target xor a list of targets """
    command = [binary()]
    command += ["--build", build_dir]
    if target:
        command += ["--target", target]
    elif targets:
        command += ["--target"]
        command += targets
    else:
        raise "Missing 'target' or 'targets' parameters"

    subprocess.check_call(command)

