#!/usr/bin/env python3
#
# Copyright (c) 2020 Kai Hoewelmeyer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from filter_files import matching_files
from difflib import unified_diff
import subprocess
import sys

FILES_TO_FORMAT = ["*.cpp", "*.h"]

"""
Prints unified diff for wrongly formatted files
Returns True when the file's formatting is ok
"""
def check_formatting(file):
    formatting_ok = True
    formatted_content = subprocess.check_output(["/usr/bin/env", "clang-format", file], text=True).splitlines(True)

    with open(file) as original_file:
        original_lines = original_file.readlines()
        diff_generator = unified_diff(original_lines,
                formatted_content,
                fromfile=file,
                tofile=file+"_clang-format")
        for line in diff_generator:
            formatting_ok = False
            sys.stdout.write(line)

    return formatting_ok



if __name__ == '__main__':
    if not all([check_formatting(f) for f in matching_files(".", FILES_TO_FORMAT)]):
        sys.exit(1)
